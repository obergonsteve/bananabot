﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Class to handle music and some SFX
// SFXAudioSource can play one clip at a time, so AudioManager only
// handles events/clips that are related to the player - ie. cannot happen sumultaneously
//
// SFX that can occur simultaneously (eg. balloons popping) are handled by the respective behaviours
public class AudioManager : MonoBehaviour
{
	[Header("Audio Sources")]
	public AudioSource IntroMusic;
	public AudioSource GameMusic;
	public AudioSource DrumBeat;
	public AudioSource SFXAudioSource;
	public AudioSource WindSource;

	[Header("Audio Clips")]
	//public AudioClip BalloonPopAudio;
	//public AudioClip WaterSplashAudio;
	public AudioClip FlipAudio;
	public AudioClip SwipeStartAudio;
	public AudioClip SwipeEndAudio;
	//public AudioClip InvalidSwipeAudio;		// 'spamming'

	public AudioClip HitGroundAudio;
	public AudioClip HitPlatformAudio;
	public AudioClip HitObstacleAudio;
	public AudioClip LandAudio;

	public AudioClip StartPlayAudio;
	public AudioClip GameOverAudio;
	public AudioClip RestartGameAudio;

	//public AudioClip DischargeAudio;
	//public AudioClip LifeLostAudio;
	//public AudioClip LifeSavedAudio;

	//public AudioClip PlatformFlashAudio;
	//public AudioClip PlatformCrumbleAudio;
	public AudioClip FeedbackAudio;
	//public AudioClip BonusScoreAudio;
	public AudioClip PersonalBestAudio;

	public AudioClip ShowOptionsAudio;
	public AudioClip HideOptionsAudio;


	public const float SFXVolume = 10f;     // for AudioSource.PlayClipAtPoint audio


	public void OnEnable()
	{
		BeatEvents.OnNewGame += OnNewGame;
		BeatEvents.OnStartPlay += OnStartPlay;
		BeatEvents.OnNewGameStarted += OnRestartGame;
		BeatEvents.OnZeroHealth += OnZeroHealth;
		BeatEvents.OnZeroFuel += OnZeroFuel;
		//BeatEvents.OnLifeLost += OnLifeLost;
		//BeatEvents.OnLifeChargeDischarge += OnLifeChargeDischarge;
		//BeatEvents.OnBalloonPopped += OnBalloonPopped;
		//BeatEvents.OnLifeGained += OnLifeGained;
		BeatEvents.OnSwipeStart += OnSwipeStart;
		BeatEvents.OnSwipeEnd += OnSwipeEnd;
		BeatEvents.OnFigureFlip += OnFigureFlip;
		BeatEvents.OnFigureHitGround += OnFigureHitGround;
		BeatEvents.OnFigureHitPlatform += OnFigureHitPlatform;
		BeatEvents.OnFigureHitObstacle += OnFigureHitObstacle;
		//BeatEvents.OnPlatformFlash += OnPlatformFlash;
		//BeatEvents.OnPlatformExpired += OnPlatformExpired;
		BeatEvents.OnBonusScoreFeedback += OnBonusScoreFeedback;
		//BeatEvents.OnBonusScoreIncrement += OnBonusScoreIncrement;
		BeatEvents.OnNewPersonalBest += OnNewPersonalBest;

		BeatEvents.OnGameOptions += OnGameOptions;
		BeatEvents.OnMusicVolumeChanged += OnMusicVolumeChanged;
		BeatEvents.OnDrumsVolumeChanged += OnDrumsVolumeChanged;
		BeatEvents.OnSFXVolumeChanged += OnSFXVolumeChanged;
	}

	public void OnDisable()
	{
		BeatEvents.OnNewGame -= OnNewGame;
		BeatEvents.OnStartPlay -= OnStartPlay;
		BeatEvents.OnNewGameStarted -= OnRestartGame;
		BeatEvents.OnZeroHealth -= OnZeroHealth;
		BeatEvents.OnZeroFuel -= OnZeroFuel;
		//BeatEvents.OnLifeLost -= OnLifeLost;
		//BeatEvents.OnLifeChargeDischarge -= OnLifeChargeDischarge;
		//BeatEvents.OnBalloonPopped -= OnBalloonPopped;
		//BeatEvents.OnLifeGained -= OnLifeGained;
		BeatEvents.OnSwipeStart -= OnSwipeStart;
		BeatEvents.OnSwipeEnd -= OnSwipeEnd;
		BeatEvents.OnFigureFlip -= OnFigureFlip;
		BeatEvents.OnFigureHitGround -= OnFigureHitGround;
		BeatEvents.OnFigureHitPlatform -= OnFigureHitPlatform;
		BeatEvents.OnFigureHitObstacle -= OnFigureHitObstacle;
		//BeatEvents.OnPlatformFlash -= OnPlatformFlash;
		//BeatEvents.OnPlatformExpired -= OnPlatformExpired;
		BeatEvents.OnBonusScoreFeedback -= OnBonusScoreFeedback;
		//BeatEvents.OnBonusScoreIncrement -= OnBonusScoreIncrement;
		BeatEvents.OnNewPersonalBest -= OnNewPersonalBest;

		BeatEvents.OnGameOptions -= OnGameOptions;
		BeatEvents.OnMusicVolumeChanged -= OnMusicVolumeChanged;
		BeatEvents.OnDrumsVolumeChanged -= OnDrumsVolumeChanged;
		BeatEvents.OnSFXVolumeChanged -= OnSFXVolumeChanged;
	}

	private void Start()
	{
		//DrumBeat.Play();
		WindSource.Play();
	}

	private void OnNewGame(GameScores scores)
	{
		IntroMusic.Stop();
		GameMusic.Play();
		WindSource.Stop();
		//DrumBeat.Stop();        // needs to restart in sync with the music
	}


	private void OnStartPlay()
	{
		PlaySFX(StartPlayAudio);
	}


	private void OnRestartGame(Action restart)
	{
		PlaySFX(RestartGameAudio);
	}

	private void OnZeroHealth()
	{
		PlaySFX(GameOverAudio);
		GameMusic.Stop();
		//DrumBeat.Stop();
	}

	private void OnZeroFuel()
	{
		PlaySFX(GameOverAudio);
		GameMusic.Stop();
		//DrumBeat.Stop();
	}

	//private void OnLifeLost(int livesLeft)
	//{
	//	PlaySFX(LifeLostAudio);
	//}

	//private void OnLifeChargeDischarge(bool saveLife)
	//{
	//	PlaySFX(DischargeAudio);
	//}

	//private void OnBalloonPopped(HeartBalloon heart)
	//{
	//	PlaySFX(BalloonPopAudio);
	//}

	//private void OnLifeGained(int livesLeft)
	//{
	//	PlaySFX(LifeSavedAudio);
	//}

	private void OnSwipeStart(Vector2 startPosition, int touchCount)
	{
		PlaySFX(SwipeStartAudio);
	}

	private void OnSwipeEnd(Vector2 endPosition, Vector2 direction, float speed, float swipeForce, int touchCount)
	{
		PlaySFX(SwipeEndAudio);
	}

	private void OnFigureFlip(StickFigure.Facing facing)
	{
		PlaySFX(FlipAudio);
	}

	private void OnFigureHitGround(Ground ground)
	{
		PlaySFX(HitGroundAudio);
	}

	private void OnFigureHitPlatform(Platform platform)
	{
		PlaySFX(HitPlatformAudio);
	}

	private void OnFigureHitObstacle(Obstacle obstacle)
	{
		PlaySFX(HitObstacleAudio);
	}

	//private void OnFigureLanded()
	//{
	//	PlaySFX(LandAudio);
	//}


	private void OnPlatformFlash(Platform platform)
	{
		//PlaySFX(PlatformFlashAudio);
	}

	private void OnBonusScoreFeedback(Color colour, Vector3 position, string message, int bonusScore)
	{
		PlaySFX(FeedbackAudio);
	}

	//private void OnBonusScoreIncrement(int bonusScore)
	//{
	//	PlaySFX(BonusScoreAudio);
	//}

	private void OnNewPersonalBest(GameScores scores, bool reloadScores)
	{
		PlaySFX(PersonalBestAudio);
	}


	private void OnGameOptions(bool showing)
	{
		if (showing)
			PlaySFX(ShowOptionsAudio);
		else
			PlaySFX(HideOptionsAudio);
	}

	private void OnMusicVolumeChanged(float vol)
	{
		IntroMusic.volume = vol;
		GameMusic.volume = vol;
	}

	private void OnDrumsVolumeChanged(float vol)
	{
		DrumBeat.volume = vol;
	}

	private void OnSFXVolumeChanged(float vol)
	{
		SFXAudioSource.volume = vol;
	}

	//private void OnFigureHitWater()
	//{
	//	PlaySFX(WaterSplashAudio);
	//}

	//private void OnFigureLeaveWater()
	//{
	//	PlaySFX(WaterSplashAudio);
	//}


	private void PlaySFX(AudioClip clip)
	{
		if (clip != null)
		{
			SFXAudioSource.clip = clip;
			SFXAudioSource.Play();
		}
	}
}
