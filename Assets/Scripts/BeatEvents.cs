﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatEvents
{
    // game / lives
    
    public delegate void OnGameStateChangedDelegate(GameManager.GameState currentState, GameManager.GameState newState);
    public static OnGameStateChangedDelegate OnGameStateChanged;

    public delegate void OnStartPlayDelegate();
    public static OnStartPlayDelegate OnStartPlay;          // on 'tap to play!' (first tap)

    public delegate void OnGameRestartDelegate();           // after life lost
    public static OnGameRestartDelegate OnGameRestart;

    public delegate void OnPlayerHealthFuelChangedDelegate(float healthChange, float healthLevel, float fuelChange, float fuelLevel);
    public static OnPlayerHealthFuelChangedDelegate OnPlayerHealthFuelChanged;

    public delegate void OnZeroHealthDelegate();
    public static OnZeroHealthDelegate OnZeroHealth;    // -> game over state

    public delegate void OnZeroFuelDelegate();
    public static OnZeroFuelDelegate OnZeroFuel;    // -> game over state

    public delegate void OnTimeRemainingChangedDelegate(float timeChange, float timeRemaining, bool init, bool pulse);
    public static OnTimeRemainingChangedDelegate OnTimeRemainingChanged;

    public delegate void OnZeroTimeDelegate();
    public static OnZeroTimeDelegate OnZeroTime;    // -> game over state

    public delegate void OnNewGameDelegate(GameScores scores);           // after game over
    public static OnNewGameDelegate OnNewGame;

    public delegate void OnStartNewGameDelegate();
    public static OnStartNewGameDelegate OnStartNewGame;            // restart / play again

    public delegate void OnNewGameStartedDelegate(Action restart);
	public static OnNewGameStartedDelegate OnNewGameStarted;

    public delegate void OnResetDataDelegate();
    public static OnResetDataDelegate OnResetData;

    public delegate void OnQuitGameDelegate();
    public static OnQuitGameDelegate OnQuitGame;

    // UI

    public delegate void OnToggleOptionsDelegate();
    public static OnToggleOptionsDelegate OnToggleOptions;

    public delegate void OnGameOptionsDelegate(bool showing);
    public static OnGameOptionsDelegate OnGameOptions;

    public delegate void OnToggleLeaderboardDelegate();
    public static OnToggleLeaderboardDelegate OnToggleLeaderboard;

    // audio

    public delegate void OnMusicVolumeChangedDelegate(float vol);
    public static OnMusicVolumeChangedDelegate OnMusicVolumeChanged;

    public delegate void OnDrumsVolumeChangedDelegate(float vol);
    public static OnDrumsVolumeChangedDelegate OnDrumsVolumeChanged;

    public delegate void OnSFXVolumeChangedDelegate(float vol);
    public static OnSFXVolumeChangedDelegate OnSFXVolumeChanged;

    // scoring

    public delegate void OnUpdateScoreDelegate(GameScores scores); 
    public static OnUpdateScoreDelegate OnUpdateScore;

    public delegate void OnScoreResetDelegate();                                        // on start new game
    public static OnScoreResetDelegate OnScoreReset;

    public delegate void OnFinalGameScoreDelegate(GameScores scores, bool newPB, GameManager.GameOverState state);           // on game over
    public static OnFinalGameScoreDelegate OnFinalGameScore;

    public delegate void OnNewPersonalBestDelegate(GameScores scores, bool reloadScores);           // on game over
    public static OnNewPersonalBestDelegate OnNewPersonalBest;

    public delegate void OnBonusScoreFeedbackDelegate(Color colour, Vector3 position, string message, int bonusScore);    
    public static OnBonusScoreFeedbackDelegate OnBonusScoreFeedback;            // UI -> update game score

    public delegate void OnBonusScoreIncrementDelegate(int bonusScore);
    public static OnBonusScoreIncrementDelegate OnBonusScoreIncrement;          // update game score

    // beat

    //public delegate void OnStartBeatDelegate();
    //public static OnStartBeatDelegate OnStartBeat;

    //public delegate void OnBeatDelegate(float BPM, long beatTicks, long lastBeatTicks);
    //public static OnBeatDelegate OnBeat;                           // hearts / UI start pump

    //public delegate void OnStartHeartBeatDelegate();
    //public static OnStartHeartBeatDelegate OnStartHeartBeat;

    //public delegate void OnHeartBeatDelegate(float BPM, long beatTicks, long lastBeatTicks);
    //public static OnHeartBeatDelegate OnHeartBeat;               // when hearts at full pump  // TODO: ???

    // stick figure

    //public delegate void OnAltitudeChangedDelegate(float altitude, float gravity, float balloonLift, float maxAltitude, GameSettings gameSettings);
    //public static OnAltitudeChangedDelegate OnAltitudeChanged;

    //public delegate void OnNewHighestAltitudeDelegate(float highestAltitude, float maxAltitude);
    //public static OnNewHighestAltitudeDelegate OnNewHighestAltitude;

    public delegate void OnFigureStateChangedDelegate(StickFigure.State oldState, StickFigure.State newState);
    public static OnFigureStateChangedDelegate OnFigureStateChanged;

    public delegate void OnFigurePositionChangedDelegate(Vector2 lastPosition, Vector2 figurePosition);
	public static OnFigurePositionChangedDelegate OnFigurePositionChanged;

	//public delegate void OnFigureLiftChangedDelegate(float balloonLift, float increment, float totalHeldBalloonLift, bool fromFloatingBalloon);
 //   public static OnFigureLiftChangedDelegate OnFigureLiftChanged;

    //public delegate void OnFigureBalloonCollisionDelegate(HeartBalloon heart, bool heldHeartCollision);
    //public static OnFigureBalloonCollisionDelegate OnFigureBalloonCollision;

    public delegate void OnFigureFlipDelegate(StickFigure.Facing facing);
    public static OnFigureFlipDelegate OnFigureFlip;

    public delegate void OnFigureHitFruitDelegate(Fruit fruit);
    public static OnFigureHitFruitDelegate OnFigureHitFruit;

    public delegate void OnFigureHitGroundDelegate(Ground ground);
    public static OnFigureHitGroundDelegate OnFigureHitGround;

    public delegate void OnFigureHitPlatformDelegate(Platform platform);
    public static OnFigureHitPlatformDelegate OnFigureHitPlatform;

    public delegate void OnFigureHitObstacleDelegate(Obstacle obstacle);
    public static OnFigureHitObstacleDelegate OnFigureHitObstacle;

    public delegate void OnFigureLandedDelegate();
    public static OnFigureLandedDelegate OnFigureLanded;

    public delegate void OnFigureHitWaterDelegate(Vector3 figurePosition);
    public static OnFigureHitWaterDelegate OnFigureHitWater;

    public delegate void OnFigureLeaveWaterDelegate(Vector3 figurePosition);
    public static OnFigureLeaveWaterDelegate OnFigureLeaveWater;

    public delegate void OnFigureImpulseDelegate(Vector3 direction, float impulse);
    public static OnFigureImpulseDelegate OnFigureImpulse;

    // fruit

    public delegate void OnThrowFruitDelegate(Fruit fruitPrefab, Vector3 origin, Vector3 direction, float impulse);
    public static OnThrowFruitDelegate OnThrowFruit;

    public delegate void OnPlayerHitFruitDelegate(float fuel, float fruit, bool isRotten, float toxicity, float secondsRemaining);
    public static OnPlayerHitFruitDelegate OnPlayerTakeFruit;

    // platforms

    public delegate void OnPlatformFlashDelegate(Platform platform);
    public static OnPlatformFlashDelegate OnPlatformFlash;

    public delegate void OnPlatformExpiredDelegate(Platform platform);
    public static OnPlatformExpiredDelegate OnPlatformExpired;           // rest time limit expired (fall under gravity)

    public delegate void OnPlatformRemovedDelegate(Platform platform);
    public static OnPlatformRemovedDelegate OnPlatformRemoved;           // hit water or another platform

    // touch input

    public delegate void OnPlayerTapDelegate(GameManager.GameState gameState, int tapCount);
    public static OnPlayerTapDelegate OnPlayerTap;

    public delegate void OnSwipeStartDelegate(Vector2 startPosition, int touchCount);      // validSwipe false if 'spamming'
    public static OnSwipeStartDelegate OnSwipeStart;

    public delegate void OnSwipeMoveDelegate(Vector2 movePosition, int touchCount);
    public static OnSwipeMoveDelegate OnSwipeMove;

    public delegate void OnOnSwipeEndDelegate(Vector2 endPosition, Vector2 direction, float speed, float swipeForce, int touchCount);   
    public static OnOnSwipeEndDelegate OnSwipeEnd;

    // life charge ('AED')

    public delegate void OnLifeChargeIncreaseDelegate(float increase);
    public static OnLifeChargeIncreaseDelegate OnLifeChargeIncrease;

    public delegate void OnLifeChargeChangedDelegate(float prevCharge, float newCharge, float maxCharge);
    public static OnLifeChargeChangedDelegate OnLifeChargeChanged;

    public delegate void OnLifeChargeIncrementDelegate(float newCharge, int chargeBlocks);
    public static OnLifeChargeIncrementDelegate OnLifeChargeIncrement;

    public delegate void OnLifeChargeDischargeDelegate(bool saveLife);      // eg. false if lightning discharge
    public static OnLifeChargeDischargeDelegate OnLifeChargeDischarge;

	// leaderboard

	public delegate void OnInternetCheckDelegate(bool internetReachable);
	public static OnInternetCheckDelegate OnInternetCheck;

	public delegate void OnLookupPlayerProfileDelegate(string playerName, string schoolName);
    public static OnLookupPlayerProfileDelegate OnLookupPlayerProfile;

    public delegate void OnLookupPlayerProfileResultDelegate(string playerName, string schoolName, bool playerFound, bool schoolFound);
    public static OnLookupPlayerProfileResultDelegate OnLookupPlayerProfileResult;


    public delegate void OnSavePlayerProfileDelegate(string playerName, string school, PlayerProfile playerProfile);
    public static OnSavePlayerProfileDelegate OnSavePlayerProfile;

    public delegate void OnSavePlayerProfileResultDelegate(string playerName, bool success, bool loadScores);
    public static OnSavePlayerProfileResultDelegate OnSavePlayerProfileResult;

    public delegate void OnLookupPlayerScoresDelegate();
    public static OnLookupPlayerScoresDelegate OnLookupPlayerScores;

    public delegate void OnLookupSchoolScoresDelegate(string schoolName);
    public static OnLookupSchoolScoresDelegate OnLookupSchoolScores;

    public delegate void OnPlayerScoresUpdatedDelegate(GameScores scores, bool success, bool reloadScores);
    public static OnPlayerScoresUpdatedDelegate OnPlayerScoresUpdated;

    public delegate void OnPlayerScoresRetrievedDelegate(List<LeaderboardScore> scores);
    public static OnPlayerScoresRetrievedDelegate OnPlayerScoresRetrieved;

    public delegate void OnSchoolScoresRetrievedDelegate(string schoolName, List<LeaderboardScore> scores);
    public static OnSchoolScoresRetrievedDelegate OnSchoolScoresRetrieved;

    public delegate void OnLookupSchoolsDelegate();
    public static OnLookupSchoolsDelegate OnLookupSchools;

    public delegate void OnSchoolsRetrievedDelegate(List<string> schools);
    public static OnSchoolsRetrievedDelegate OnSchoolsRetrieved;
}
