﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class BeatKeeper : MonoBehaviour
//{
//    public float BPM = 100;
//    public AudioClip BeatClick;

//    public bool ClickTrack;

//    private bool beatStarted = false;

//    private float SecondsPerBeat => 60f / BPM;
//    private float TicksPerBeat => (SecondsPerBeat * TimeSpan.TicksPerSecond);

//    private long lastBeatTimeTicks = 0;
//    private float ticksSinceLastBeat = 0f;
//    private int beatCounter = 0;

//    private long lastHeartBeatTimeTicks = 0;
//    private float ticksSinceLastHeartBeat = 0f;
//    private int heartBeatCounter = 0;

//    // timings for pump animations
//    public static float PumpInTime = 0.3f; //0.5f;           // BPM factor
//    public static float PumpOutTime = 0.4f; //0.25f;         // BPM factor


//    public void OnEnable()
//    {
//        BeatEvents.OnStartBeat += OnStartBeat;
//    }

//    public void OnDisable()
//    {
//        BeatEvents.OnStartBeat -= OnStartBeat;
//    }


//    private void OnStartBeat()
//	{
//        beatStarted = true;
//        lastBeatTimeTicks = DateTime.Now.Ticks;

//        beatCounter = 0;
//        heartBeatCounter = 0;

//        ticksSinceLastBeat = 0f;
//        ticksSinceLastHeartBeat = (TicksPerBeat * (1f - PumpInTime));        // to sync with balloon pulse time)

//        //Debug.Log($"BeatKeeper: ticksSinceLastBeat = {ticksSinceLastBeat} TicksPerBeat = {TicksPerBeat}");
//    }

//    // fixedDeltaTime = 0.02 secs
//    private void FixedUpdate()
//	{
//        if (!beatStarted)
//            return;

//        ticksSinceLastBeat += Time.deltaTime * TimeSpan.TicksPerSecond;    
//        ticksSinceLastHeartBeat += Time.deltaTime * TimeSpan.TicksPerSecond;   

//        // OnBeat - start of every beat
//        if (ticksSinceLastBeat >= TicksPerBeat)
//        {
//            beatCounter++;
//            long beatTimeTicks = DateTime.Now.Ticks;

//			//Debug.Log($"BeatKeeper OnBeat: ticksSinceLastBeat = {ticksSinceLastBeat} TicksPerBeat = {TicksPerBeat} Time.deltaTime = {Time.deltaTime}");

//			BeatEvents.OnBeat?.Invoke(BPM, beatTimeTicks, lastBeatTimeTicks);

//            lastBeatTimeTicks = beatTimeTicks;
//            ticksSinceLastBeat = 0f;
//        }

//        // OnHeartBeat - point during beat when hearts are pumped up
//        if (ticksSinceLastHeartBeat >= TicksPerBeat)
//        {
//            heartBeatCounter++;
//            long heartBeatTimeTicks = DateTime.Now.Ticks;
//			//Debug.Log($"BeatKeeper OnHeartBeat: ticksSinceLastHeartBeat = {ticksSinceLastHeartBeat} TicksPerBeat = {TicksPerBeat} Time.deltaTime = {Time.deltaTime}");

//			BeatEvents.OnHeartBeat?.Invoke(BPM, heartBeatTimeTicks, lastBeatTimeTicks);

//            if (heartBeatCounter == 1)
//                BeatEvents.OnStartHeartBeat?.Invoke();

//            if (ClickTrack && BeatClick != null)
//                AudioSource.PlayClipAtPoint(BeatClick, Vector3.zero, AudioManager.SFXVolume);

//            lastHeartBeatTimeTicks = heartBeatTimeTicks;
//            ticksSinceLastHeartBeat = 0f;
//        }
//    }
//}
