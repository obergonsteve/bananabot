﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bunch : MonoBehaviour
{
    public Fruit FruitPrefab;
    public List<Fruit> Fruits;

    public float FruitOffset = 0.5f;

    public void Spawn(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var newFruit = Instantiate(FruitPrefab, transform);
        }

    }
}
