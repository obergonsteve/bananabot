﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// TODO: pool, move, scale, transparency fluctuations/animations

[RequireComponent(typeof(SpriteRenderer))]
public class Cloud : MonoBehaviour
{
	private SpriteRenderer CloudSprite;

	private void Awake()
	{
		CloudSprite = GetComponent<SpriteRenderer>();
	}

	public void SetTransparency(float trans)
	{
		CloudSprite.color = new Color(1, 1, 1, trans);
	}

}
