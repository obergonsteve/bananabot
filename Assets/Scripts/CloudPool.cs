﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudPool : MonoBehaviour
{
    public Transform Near;          // in front of balloons / figure
    public Transform Far;           // behind balloons / figure

    public List<Cloud> Pool;
    public Cloud CloudPrefab;

    public GameSettings gameSettings;


    public void OnEnable()
    {

    }

    public void OnDisable()
    {

    }

    public Cloud GetRandomCloud()
    {
        bool isNear = Random.Range(0.0f, 1.0f) < 0.5f;
        float cloudScale = Random.Range(gameSettings.minCloudScale, gameSettings.maxCloudScale);
        float transparency = Random.Range(gameSettings.minCloudTransparancy, 1.0f);

        return GetCloud(isNear, cloudScale, cloudScale, transparency);

    }

    public Cloud GetCloud(bool near, float xScale = 1, float yScale = 1, float transparency = 1)
    {
        Cloud foundCloud = null;

        foreach (var cloud in Pool)
        {
            if (!cloud.gameObject.activeSelf)
            {
                cloud.gameObject.SetActive(true);
                cloud.transform.parent = near ? Near : Far;
                foundCloud = cloud;
                break;
            }
        }

        if (foundCloud == null)
        {
            foundCloud = Instantiate(CloudPrefab, near ? Near : Far);
            Pool.Add(foundCloud);
        }

        foundCloud.transform.localScale = new Vector2(xScale, yScale);
        foundCloud.SetTransparency(transparency);

        return foundCloud;
    }


    public void PoolCloud(Cloud cloud)
    {
        cloud.transform.parent = transform;
        cloud.gameObject.SetActive(false);      // ie. pooled
    }
}
