﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DaylightChanger : MonoBehaviour
{
	public Light SunLight;  
	public Light MoonLight;

	public Light SkyLight1;
	public Light SkyLight2;
	public Light GroundLight1;
	public Light GroundLight2;

	public float SecondsPerDay = 60f;		// time in seconds for lights to rotate 360 deg (ie. 24 hours)
	public int SunRiseHour = 6;    

	public Color sunMiddayColour;
	public Color sunDuskDawnColour;

	public Color moonMidnightColour;
	public Color moonDuskDawnColour;

	//public AnimationCurve IntensityCurve;

	private float MaxSunIntensity;
	private float MaxMoonIntensity;

	private float MaxSkyLight1Intensity;
	private float MaxSkyLight2Intensity;

	private float MaxGroundLight1Intensity;
	private float MaxGroundLight2Intensity;

	private bool isDaytime;

	private int dayCount;
	private int timeOfDay;

	private bool countingHours;


	//public void OnEnable()
	//{
	//	BeatEvents.OnAltitudeChanged += OnAltitudeChanged;
	//}

 //   public void OnDisable()
	//{
	//	BeatEvents.OnAltitudeChanged -= OnAltitudeChanged;
	//}

	private void Start()
	{
		SunLight.color = sunDuskDawnColour;
		MoonLight.color = moonDuskDawnColour;

		MaxSunIntensity = SunLight.intensity;
		MaxMoonIntensity = MoonLight.intensity;

		MaxSkyLight1Intensity = SkyLight1.intensity;
		MaxSkyLight2Intensity = SkyLight2.intensity;

		MaxGroundLight1Intensity = GroundLight1.intensity;
		MaxGroundLight2Intensity = GroundLight2.intensity;

		SetDaytime(true);
		StartCoroutine(HourCounter());

		// rotate sun and moon around x axis	
		RotateSun();
		RotateMoon();

		CycleSunColours();
		CycleMoonColours();
	}


	//private void OnAltitudeChanged(float altitude, float gravity, float balloonLift, float maxAltitude, GameSettings gameSettings)
	//{
		//SunLight.intensity = MaxSunIntensity * (1 - (altitude / maxAltitude));
		//MoonLight.intensity = MaxMoonIntensity * (1 - (altitude / maxAltitude));

		//SkyLight1.intensity = MaxSkyLight1Intensity * (1 - (altitude / maxAltitude));
		//SkyLight2.intensity = MaxSkyLight2Intensity * (1 - (altitude / maxAltitude));

		//GroundLight1.intensity = MaxGroundLight1Intensity * (1 - (altitude / maxAltitude));
		//GroundLight2.intensity = MaxGroundLight2Intensity * (1 - (altitude / maxAltitude));
	//}


	private void SetDaytime(bool daytime)
	{
		if (daytime)
		{
			//BeatEvents.OnSunrise?.Invoke(); 
			isDaytime = true;
		}
		else
		{
			//Debug.Log("Sunset");
			//BeatEvents.OnSunset?.Invoke();
			isDaytime = false;
		}
	}


	private IEnumerator HourCounter()
	{
		if (countingHours)
			yield break;

		countingHours = true;

		dayCount = 1;
		timeOfDay = SunRiseHour;
		//BeatEvents.OnDayCount?.Invoke(dayCount);
		//BeatEvents.OnHourCount?.Invoke(timeOfDay);

		int dayHourCount = 0;	// 0-23

		while (countingHours)
		{
			// wait 1 'game hour'
			yield return new WaitForSecondsRealtime(SecondsPerDay / 24f);

			dayHourCount++;
			timeOfDay++;

			if (timeOfDay == 24)
			{
				timeOfDay = 0;
			}

			if (dayHourCount >= 24)
			{
				dayCount++;
				dayHourCount = 0;
				//BeatEvents.OnDayCount?.Invoke(dayCount);
			}

			//BeatEvents.OnHourCount?.Invoke(timeOfDay);
		}
	}

	private void RotateSun()
	{
		LeanTween.rotateAround(SunLight.gameObject, SunLight.transform.right, 180f, SecondsPerDay / 2f)
		//LeanTween.rotateAround(SunLight.gameObject, Vector3.right, 180f, SecondsPerDay / 2f)
				.setEase(LeanTweenType.linear)
				.setOnComplete(() =>
				{
					SetDaytime(!isDaytime);
					RotateSun();
				});
	}

	private void RotateMoon()
	{
		LeanTween.rotateAround(MoonLight.gameObject, MoonLight.transform.right, 180f, SecondsPerDay / 2f)
		//LeanTween.rotateAround(MoonLight.gameObject, Vector3.right, 180f, SecondsPerDay / 2f)
				 .setEase(LeanTweenType.linear)
				.setOnComplete(() =>
				{
					RotateMoon();
				});
	}

	private void CycleSunColours()
	{
		LeanTween.value(gameObject, sunDuskDawnColour, sunMiddayColour, SecondsPerDay / 4f)
				.setEase(LeanTweenType.linear)
				.setOnUpdate((Color val) =>
				{
					SunLight.color = val;
				})
				.setLoopPingPong();
	}

	private void CycleMoonColours()
	{
		LeanTween.value(gameObject, moonDuskDawnColour, moonMidnightColour, SecondsPerDay / 4f)
				.setEase(LeanTweenType.linear)
				.setOnUpdate((Color val) =>
				{
					MoonLight.color = val;
				})
				.setLoopPingPong();
	}
}
