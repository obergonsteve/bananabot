﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureAnimation : MonoBehaviour
{
	//private void OnTriggerEnter(Collider collision)
	//{
	//	//Debug.Log("FigureAnimation.OnTriggerEnter " + collision.gameObject.name);

	//	if (collision.gameObject.CompareTag("Balloon"))
	//	{
	//		var otherHeart = collision.gameObject.GetComponent<HeartBalloon>();
	//		if (otherHeart != null && otherHeart.CanFigurePop)     // ie. held balloon (which deflates) hits floating balloon
	//		{
	//			otherHeart.FigureIsPopping = true;
	//			//Debug.Log("FigureAnimation.OnTriggerEnter hit balloon " + collision.gameObject.name);
	//			BeatEvents.OnFigureBalloonCollision?.Invoke(otherHeart, false);
	//		}
	//	}
	//	else if (collision.gameObject.CompareTag("Water"))
	//	{
	//		//Debug.Log("Hit Water!");
	//		BeatEvents.OnFigureHitWater?.Invoke(transform.position);
	//	}
	//}

	//private void OnTriggerExit(Collider collision)
	//{
	//	//Debug.Log("FigureAnimation.OnTriggerExit " + collision.gameObject.name);

	//	if (collision.gameObject.CompareTag("Water"))
	//	{
	//		//Debug.Log("Leaving Water!");
	//		BeatEvents.OnFigureLeaveWater?.Invoke(transform.position);
	//	}
	//}

	// animator event
	public void OnLanded()
	{
		BeatEvents.OnFigureLanded?.Invoke();
	}
}
