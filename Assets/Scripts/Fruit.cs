﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour
{
	public float RotTime = 10f;
	public Gradient RotColourGradient;

	public float MaxFuel { get; private set; }
	public float MaxHealth { get; private set; }

	public float SpawnTime;
	public float ShrinkTime;
	public float FallTime = 0.5f;
	public float GrowScale;
	public float ThrowTorque;

	public AudioClip SpawnAudio;

	[Header("Timer")]
	public AudioClip TimeTickAudio;

	private float ClockTime;            // initial time

	public float TickScale = 2f;
	public float TickTime = 0.25f;

	private float timerInterval = 1f;       // seconds
	private Coroutine timeCountdownCoroutine;
	private bool timerRunning = false;

	private float secondsRemaining;
	private float timeFactor;       // multiplied by SecondsRemaining when fruit taken by player

	// health and fuel values decrease with rotting
	public float HealthValue => MaxHealth - Toxicity;
	public float FuelValue => MaxFuel - (MaxFuel * rotLevel);
	public float Toxicity => MaxHealth * rotLevel;
	public bool IsRotten { get; private set; }

	public AudioClip HitGroundAudio;
	public AudioClip HitFruitAudio;
	public AudioClip HitPlayerAudio;
	public AudioClip ThrowAudio;

	private bool isThrown = false;
	private bool onGround = false;
	private bool onPlatform = false;
	private float rotLevel = 0f;        // from 0 (fresh) to 1 (black)

	private bool gameOver = false;

	private SpriteRenderer spriteRenderer;
	private Rigidbody2D rigidBody;


	private void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		rigidBody = GetComponent<Rigidbody2D>();
	}

	private void OnEnable()
	{
		BeatEvents.OnNewGame += OnNewGame;
		BeatEvents.OnGameStateChanged += OnGameStateChanged;
	}

	private void OnDisable()
	{
		BeatEvents.OnNewGame -= OnNewGame;
		BeatEvents.OnGameStateChanged -= OnGameStateChanged;
	}

	// set initial fuel, health and launch parameters from SpawnPoint
	private void Init(SpawnPoint spawnPoint)
	{
		//RotTime = spawnPoint.RotTime;
		MaxFuel = spawnPoint.MaxFuel;
		MaxHealth = spawnPoint.MaxHealth;

		ClockTime = spawnPoint.ClockTime;
		timeFactor = spawnPoint.TimeFactor;
		secondsRemaining = 0;


		if (spawnPoint.LaunchForce != Vector2.zero)
			rigidBody.AddForce(spawnPoint.LaunchForce, ForceMode2D.Impulse);

		rigidBody.gravityScale = spawnPoint.GravityScale;
	}


	private void OnNewGame(GameScores scores)
	{
		gameOver = false;
	}

	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
	{
		gameOver = (newState == GameManager.GameState.GameOver);

		if (gameOver)
			timerRunning = false;
	}

	private void StartRotting()
	{
		if (IsRotten)
			return;

		spriteRenderer.color = RotColourGradient.Evaluate(0f);

		rotLevel = 0f;

		LeanTween.value(gameObject, 0f, 1f, RotTime)
				.setEaseInCubic()
				.setOnUpdate((float f) =>
				{
					rotLevel = f;
					spriteRenderer.color = RotColourGradient.Evaluate(f);
				})
				.setOnComplete(() =>
				{
					// start falling gradually
					LeanTween.value(gameObject, rigidBody.gravityScale, 1f, FallTime)
							.setEaseInCubic()
							.setOnUpdate((float f) =>
							{
								rigidBody.gravityScale = f;
							});

					IsRotten = true;
				});
	}

	public void Grow(bool thrown)
	{
		var fruitScale = transform.localScale;
		var largeScale = fruitScale * GrowScale;
		transform.localScale = Vector3.zero;

		if (thrown)
			rigidBody.gravityScale = 1f;

		if (!thrown)
		{
			LeanTween.scale(gameObject, largeScale, SpawnTime)
				.setEaseInBack()
				.setOnComplete(() =>
				{
					LeanTween.scale(gameObject, fruitScale, SpawnTime)
						.setEaseOutBack()
						.setOnComplete(() =>
						{
							if (RotTime > 0)
								StartRotting();

							if (ClockTime > 0)
								timeCountdownCoroutine = StartCoroutine(TimeCountdown());
						});
				});
		}
		else
		{
			// scale up faster if thrown
			LeanTween.scale(gameObject, fruitScale, SpawnTime * 0.5f)
							.setEaseOutBack();
		}
	}

	public void Shrink(bool destroy)
	{
		LeanTween.scale(gameObject, Vector3.zero, ShrinkTime)
					.setEaseInBack()
					.setOnComplete(() =>
					{
						if (destroy)
							Destroy(gameObject);
					});

	}

	private IEnumerator TimeCountdown()
	{
		if (timerRunning)
			yield break;

		timerRunning = true;
		secondsRemaining = ClockTime;

		while (timerRunning)
		{
			yield return new WaitForSeconds(timerInterval);

			secondsRemaining -= timerInterval;
			//BeatEvents.OnTimeRemainingChanged?.Invoke(timeRemaining, false);

			var fruitScale = transform.localScale;
			var largeScale = fruitScale * TickScale;

			LeanTween.scale(gameObject, largeScale, TickTime)
					.setEaseInBack()
					.setOnComplete(() =>
					{
						if (TimeTickAudio != null)
							AudioSource.PlayClipAtPoint(TimeTickAudio, Vector3.zero, AudioManager.SFXVolume);

						LeanTween.scale(gameObject, fruitScale, TickTime)
							.setEaseOutBack()
							.setOnComplete(() =>
							{

							});
					});

			if (secondsRemaining <= 0f)
			{
				secondsRemaining = 0f;

				yield return null;
				timerRunning = false;
				//BeatEvents.OnZeroTime?.Invoke();
				//GameOver(GameOverState.ZeroTime);
			}
		}

		yield return null;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Ground"))
		{
			HitGround();
		}

		if (collision.gameObject.CompareTag("Platform"))
		{
			HitPlatform();
		}

		// fruit hit by thrown fruit is same as player colliding with it
		if (collision.gameObject.CompareTag("Fruit"))
		{
			//Debug.Log($"Fruit {name} hit Fruit!");
			if (isThrown)
			{
				var otherFruit = collision.gameObject.GetComponent<Fruit>();

				if (! gameOver)
					BeatEvents.OnPlayerTakeFruit?.Invoke(otherFruit.FuelValue, otherFruit.HealthValue, otherFruit.IsRotten, otherFruit.Toxicity, otherFruit.secondsRemaining);

				Shrink(true);
				otherFruit.Shrink(true);

				if (HitFruitAudio != null)
					AudioSource.PlayClipAtPoint(HitFruitAudio, transform.position, AudioManager.SFXVolume);
			}
		}

		if (collision.gameObject.CompareTag("Player"))
		{
			//Debug.Log($"Fruit {name} hit Player!");

			if (!isThrown || onGround)
			{
				if (!gameOver)
					BeatEvents.OnPlayerTakeFruit?.Invoke(FuelValue, HealthValue, IsRotten, Toxicity, secondsRemaining * timeFactor);

				Shrink(true);

				if (HitPlayerAudio != null)
					AudioSource.PlayClipAtPoint(HitPlayerAudio, transform.position, AudioManager.SFXVolume);
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Background"))
		{
			//Debug.Log($"Fruit {name} hit Background!");
			//Shrink(true);
		}
	}

	private void HitGround()
	{
		if (!onGround)
		{
			Land();
			onGround = true;
		}
	}

	private void HitPlatform()
	{
		if (!onPlatform)
		{
			Land();
			onPlatform = true;
		}
	}

	private void Land()
	{
		// TODO: splat animation?

		rigidBody.velocity = Vector2.zero;
		rigidBody.angularVelocity = 0;

		//Debug.Log($"Fruit {name} hit Ground!");
		if (isThrown)
			StartRotting();

		if (HitGroundAudio != null)
			AudioSource.PlayClipAtPoint(HitGroundAudio, transform.position, AudioManager.SFXVolume);
	}

	//public void Throw(float force, Vector2 direction)
	//{
	//	isThrown = true;
	//	rigidBody.AddForce(direction * force, ForceMode2D.Impulse);
	//	rigidBody.AddTorque(ThrowTorque * force, ForceMode2D.Impulse);

	//	if (ThrowAudio != null)
	//		AudioSource.PlayClipAtPoint(ThrowAudio, transform.position, AudioManager.SFXVolume);
	//}

	public void Throw(Vector2 throwPoint, Vector2 direction, float force)
	{
		transform.position = throwPoint;

		Grow(true);
		//Throw(force, direction);

		isThrown = true;
		rigidBody.AddForce(direction * force, ForceMode2D.Impulse);
		rigidBody.AddTorque(ThrowTorque * force, ForceMode2D.Impulse);

		if (ThrowAudio != null)
			AudioSource.PlayClipAtPoint(ThrowAudio, transform.position, AudioManager.SFXVolume);
	}

	public void Spawn(SpawnPoint spawnPoint)
	{
		Debug.Log($"Spawn: {spawnPoint.name}");

		Init(spawnPoint);
		Grow(false);

		if (SpawnAudio != null)
			AudioSource.PlayClipAtPoint(SpawnAudio, transform.position, AudioManager.SFXVolume);
	}
}