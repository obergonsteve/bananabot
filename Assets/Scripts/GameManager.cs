﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
	public List<string> LevelNames;
	private int currentLevelIndex = 0;
	public string CurrentLevel => LevelNames[currentLevelIndex];

	public GameScores GameScores;

	public enum GameState
	{
		NotStarted = 0,
		ReadyToStart,       // 'tap to play!'
		InPlay,             // in air / on platform
		InWater,            // if not game over	
		Paused,             // by the player
		GameOver            // no health / fuel left
	}
	public GameState CurrentState;

	public enum GameOverState
	{
		None = 0,
		ZeroFuel,
		ZeroHealth,  
		ZeroTime
	}

	public AudioClip TimeTickAudio;
	public float TimeLimit;					// seconds
	private float timeRemaining = 0;		// seconds
	private float timerInterval = 1f;		// seconds
	private Coroutine timeCountdownCoroutine;
	private bool timerRunning = false;

	public bool OptionsShowing;

	private DateTime focusLostTime;
	private DateTime pauseStartTime;

	public TimeSpan pausedTime = TimeSpan.Zero;     // lost focus + paused


	public void OnEnable()
	{
		BeatEvents.OnPlayerTap += OnPlayerTap;
		BeatEvents.OnFigureStateChanged += OnFigureStateChanged;
		BeatEvents.OnPlayerTakeFruit += OnPlayerTakeFruit;
		BeatEvents.OnPlayerHealthFuelChanged += OnPlayerHealthFuelChanged;
		BeatEvents.OnTimeRemainingChanged += OnTimeRemainingChanged;

		BeatEvents.OnZeroHealth += OnZeroHealth;
		BeatEvents.OnZeroFuel += OnZeroFuel;
		//BeatEvents.OnZeroTime += OnZeroTime;
		//BeatEvents.OnBonusScoreIncrement += OnBonusScoreIncrement;
		BeatEvents.OnGameOptions += OnGameOptions;
		BeatEvents.OnStartNewGame += StartNewGame;

		//GameScores.Init();
	}

	public void OnDisable()
	{
		BeatEvents.OnPlayerTap -= OnPlayerTap;
		BeatEvents.OnFigureStateChanged -= OnFigureStateChanged;
		BeatEvents.OnPlayerTakeFruit -= OnPlayerTakeFruit;
		BeatEvents.OnPlayerHealthFuelChanged -= OnPlayerHealthFuelChanged;
		BeatEvents.OnTimeRemainingChanged -= OnTimeRemainingChanged;

		BeatEvents.OnZeroHealth -= OnZeroHealth;
		BeatEvents.OnZeroFuel -= OnZeroFuel;
		//BeatEvents.OnZeroTime -= OnZeroTime;
		//BeatEvents.OnBonusScoreIncrement -= OnBonusScoreIncrement;
		BeatEvents.OnGameOptions -= OnGameOptions;
		BeatEvents.OnStartNewGame -= StartNewGame;
	}


	// disable Debug.Log messages when not building for the Unity editor (for performance)
	private void Awake()
	{
#if UNITY_EDITOR
		Debug.unityLogger.logEnabled = true;
#else
		Debug.unityLogger.logEnabled = false;
#endif

		LeanTween.init(3200);
	}

	private void Start()
	{
		SetState(GameState.ReadyToStart);
	}

	// player tap used to start play as well as swipes
	private void OnPlayerTap(GameState gameState, int tapcount)
	{
		switch (gameState)
		{
			case GameState.NotStarted:
				break;

			case GameState.ReadyToStart:
				NewGame();
				break;

			case GameState.InPlay:
				break;

			case GameState.InWater:
				//BeatEvents.OnGameRestart?.Invoke();
				break;

			case GameState.Paused:
				break;

			case GameState.GameOver:
				//StartNewGame();
				break;
		}
	}

	//private void OnBeat(float BPM, long beatTicks, long lastBeatTicks)
	//{
	//	if (CurrentState == GameState.InWater)
	//		pausedTime += TimeSpan.FromTicks(beatTicks - lastBeatTicks);

	//	GameScores.SetGamePlayTime(pausedTime);

	//	if (CurrentState == GameState.InPlay)
	//		BeatEvents.OnUpdateScore?.Invoke(GameScores, BPM);
	//}


	// on 'tap to play'
	private void NewGame()
	{
		GameScores.PlayCount++;

		GameScores.GameReset();
		BeatEvents.OnScoreReset?.Invoke();

		SetState(GameState.InPlay);
		BeatEvents.OnNewGame?.Invoke(GameScores);

		//BeatEvents.OnStartBeat?.Invoke();

		timeRemaining = TimeLimit;
		BeatEvents.OnTimeRemainingChanged?.Invoke(0, timeRemaining, true, false);

		timerRunning = false;
		timeCountdownCoroutine = StartCoroutine(TimeCountdown());
	}

	private void StartNewGame()
	{
		BeatEvents.OnNewGameStarted?.Invoke(ReloadScene);
	}

	private void ReloadScene()
	{
		Scene currentScene = SceneManager.GetActiveScene();
		SceneManager.LoadScene(currentScene.name);
	}

	private void SetState(GameState newState)
	{
		if (CurrentState == newState)
			return;

		//Debug.Log("GameManager.SetState " + newState);

		BeatEvents.OnGameStateChanged?.Invoke(CurrentState, newState);
		CurrentState = newState;
	}

	private void OnPlayerTakeFruit(float fruitFuel, float fruitHealth, bool isRotten, float fruitToxicity, float secondsRemaining)
	{
		//Debug.Log($"OnPlayerTakeFruit: fruitFuel = {fruitFuel}, fruitHealth = {fruitHealth}, fruitToxicity = {fruitToxicity}");

		timeRemaining += secondsRemaining;
		timeRemaining = Mathf.Clamp(timeRemaining, 0f, TimeLimit);
		BeatEvents.OnTimeRemainingChanged?.Invoke(secondsRemaining, timeRemaining, false, false);
	}

	private IEnumerator TimeCountdown()
	{
		if (timerRunning)
			yield break;

		timerRunning = true;

		while (timerRunning)
		{
			yield return new WaitForSeconds(timerInterval);

			timeRemaining -= timerInterval;
			BeatEvents.OnTimeRemainingChanged?.Invoke(-timerInterval, timeRemaining, false, true);

			if (TimeTickAudio != null)
				AudioSource.PlayClipAtPoint(TimeTickAudio, Vector3.zero, AudioManager.SFXVolume);

			if (timeRemaining <= 0f)
			{
				timeRemaining = 0f;
				BeatEvents.OnZeroTime?.Invoke();
				GameOver(GameOverState.ZeroTime);
			}
		}

		yield return null;
	}

	private void OnZeroHealth()
	{
		GameOver(GameOverState.ZeroHealth);
	}

	private void OnZeroFuel()
	{
		GameOver(GameOverState.ZeroFuel);
	}

	private void GameOver(GameOverState state)
	{
		if (CurrentState == GameState.GameOver)
			return;

		SetState(GameState.GameOver);
		//playEndTime = DateTime.Now;

		GameScores.SetGamePlayTime(pausedTime);
		bool newPB = GameScores.UpdatePersonalBest();

		BeatEvents.OnFinalGameScore?.Invoke(GameScores, newPB, state);

		if (newPB)
			BeatEvents.OnNewPersonalBest?.Invoke(GameScores, false);

		if (timeCountdownCoroutine != null)
		{
			StopCoroutine(timeCountdownCoroutine);
			timeCountdownCoroutine = null;
		}
		timerRunning = false;
	}


	private void OnPlayerHealthFuelChanged(float healthChange, float healthLevel, float fuelChange, float fuelLevel)
	{
		if (healthChange > 0)
			GameScores.GameHealthGained += healthChange;

		if (fuelChange > 0)
			GameScores.GameFuelGained += fuelChange;

		if (healthChange > 0 || fuelChange > 0)
			BeatEvents.OnUpdateScore?.Invoke(GameScores);
	}


	private void OnTimeRemainingChanged(float timeChange, float timeRemaining, bool init, bool pulse)
	{
		if (timeChange > 0)
		{
			GameScores.GameTimeGained += timeChange;
			BeatEvents.OnUpdateScore?.Invoke(GameScores);
		}
	}

	//private void OnBonusScoreIncrement(int bonusIncrement)
	//{
	//	GameScores.GameBonusScoreTotal += bonusIncrement;
	//}


	private void OnFigureStateChanged(StickFigure.State oldState, StickFigure.State newState)
	{
		switch (newState)
		{
			case StickFigure.State.None:
				break;
			case StickFigure.State.Standing:
				break;

			case StickFigure.State.Floating:
				SetState(GameState.InPlay);
				break;

			case StickFigure.State.Flapping:
				break;
			case StickFigure.State.Falling:
				break;
			case StickFigure.State.Landing:
				break;

			case StickFigure.State.InWater:
				SetState(GameState.InWater);
				break;
		}
	}


	private void OnGameOptions(bool show)
	{
		OptionsShowing = show;
	}


	// TODO: do these get called at app startup?
	private void OnApplicationFocus(bool focus)
	{
		//if (!focus)
		//	focusLostTime = DateTime.Now;
		//else
		//	pausedTime += DateTime.Now.Subtract(focusLostTime);
	}

	private void OnApplicationPause(bool paused)
	{
		//if (paused)
		//	pauseStartTime = DateTime.Now;
		//else
		//	pausedTime += DateTime.Now.Subtract(pauseStartTime);
	}

	private void OnApplicationQuit()
	{

	}
}
