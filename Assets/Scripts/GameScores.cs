﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameScores", menuName = "GameScores", order = 1)]
public class GameScores : ScriptableObject
{
    public int PlayCount = 0;           // total number of times game started

    public DateTime PlayStartTime;      // play start timestamp 

    [Header("Current game scores")]
    public int GamePlayTime;           // play start -> game over (seconds)
    public float GameHealthGained;
    public float GameFuelGained;
    public float GameTimeGained;

    [Header("Personal best scores")]
	public int PBPlayTime;
	public float PBHealthGained;
    public float PBFuelGained;
    public float PBTimeGained;

    public long PersonalBest;            // total score (calculated below)


    // reset on starting new game
    public void GameReset()
    {
        GameHealthGained = 0;
        GameFuelGained = 0;
        GameTimeGained = 0;

        PlayStartTime = DateTime.Now;
    }

    //public long RunningScore => (long)(GameAltitude + GameLifeCharged + (GamePlayTime * 10) + GameLivesSaved + GameBonusScoreTotal);
    public long RunningScore => (long)((GameHealthGained + (GameFuelGained > 0 ? GameFuelGained : 1f) + (GameTimeGained > 0 ? GameTimeGained : 1f))); // + (GamePlayTime * 10));

    public string AnalyticsData => string.Format("Time: {0} {1}\nScore: {2}\nPlayTime: {3}\nGameHealthGained: {4}\nGameFuelGained: {5}\nGameTimeGained: {6}\nPersonalBest: {}", DateTime.UtcNow.ToShortDateString(), DateTime.UtcNow.ToShortTimeString(), RunningScore, GamePlayTime, GameHealthGained, GameFuelGained, GameTimeGained, PersonalBest);
    //public string AnalyticsData => string.Format("Time: {0} {1}\nScore: {2}\nPlayTime: {3}\nLivesSaved: {4}\nPersonalBest: {5}", DateTime.UtcNow.ToShortDateString(), DateTime.UtcNow.ToShortTimeString(), RunningScore, GamePlayTime, GameLivesSaved, PersonalBest);


    public void OnEnable()
    {
        BeatEvents.OnResetData += OnResetData;
    }

    public void OnDisable()
    {
        BeatEvents.OnResetData -= OnResetData;
    }


    public void SetGamePlayTime(TimeSpan pausedTime)
    {
        //Debug.Log("SetGamePlayTime: pausedTime = " + pausedTime.TotalSeconds + " PlayStartTime" + PlayStartTime.ToLongTimeString());
        TimeSpan span = DateTime.Now.Subtract(PlayStartTime).Subtract(pausedTime);
        GamePlayTime = (int) span.TotalSeconds;
    }


    // update PB scores on all lives lost
    // return true if new PB
    public bool UpdatePersonalBest()
    {
        if (GameHealthGained > PBHealthGained)
        {
            PBHealthGained = GameHealthGained;
        }

        if (GameFuelGained > PBFuelGained)
        {
            PBFuelGained = GameFuelGained;
        }

        if (GameTimeGained > PBTimeGained)
        {
            PBTimeGained = GameTimeGained;
        }

        if (GamePlayTime > PBPlayTime)
        {
            PBPlayTime = GamePlayTime;
        }

        if (RunningScore > PersonalBest)
        {
            PersonalBest = RunningScore;
            return true;
        }

        return false;
    }


    private void OnResetData()
    {
        GameReset();

        PBHealthGained = 0;
        PBFuelGained = 0;
        PBTimeGained = 0;

        PersonalBest = 0;

        PlayCount = 0;
        PlayStartTime = DateTime.MinValue;
    }
}