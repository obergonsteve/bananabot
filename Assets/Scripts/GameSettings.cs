﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettingsSO", menuName = "CustomSO", order = 1)]
public class GameSettings : ScriptableObject
{
    public float maxHeight;
    public int maxBalloonCount;
    public int balloonCountHeightModifier;
    public int platformCount;
    public int cloudCount;
    public float gridSize;
    public int gridRangeToSpawn; //This is how many grid positions away from the character balloons and platforms will spawn
    public AnimationCurve balloonSpawnCurve;
    public AnimationCurve platformSpawnCurve;

    public float minCloudScale = 0.5f;
    public float maxCloudScale = 1.5f;
    public float minCloudTransparancy = 0.4f;

    public int maxDeflateInterval = 6;

    public float MusicVolume = 0.5f;
    public float DrumsVolume = 0.5f;
    public float SFXVolume = 0.5f;
}
