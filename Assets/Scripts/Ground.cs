﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
	public AudioClip LandAudio;


	public void OnEnable()
	{
		BeatEvents.OnFigurePositionChanged += OnFigurePositionChanged;
	}

	public void OnDisable()
	{
		BeatEvents.OnFigurePositionChanged -= OnFigurePositionChanged;

	}

	// move ground left/right to be directly under stick figure
	private void OnFigurePositionChanged(Vector2 lastPosition, Vector2 figurePosition)
	{
		transform.position = new Vector3(figurePosition.x, transform.position.y, transform.position.z);
	}
}
