﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
////using System.Linq;
//using UnityEngine;

//public class HeartPool : MonoBehaviour
//{
//    public List<HeartBalloon> Pool;
//    //public HeartBalloon HeartPrefab;
//    public List<HeartBalloon> HeartPrefabs;
//    public HeartMaterials HeartMaterials;

//    public float HeartPoolDelay;           // to allow particles to play out

//    public enum HeartColour
//    {
//        Yellow,
//        Blue,
//        Pink,
//        Green,
//        Purple,
//        Silver,
//        Gold,
//        Black
//    }

//    public void OnEnable()
//    {
//		BeatEvents.OnBalloonPopped += OnBalloonPopped;
//    }

//    public void OnDisable()
//    {
//		BeatEvents.OnBalloonPopped -= OnBalloonPopped;
//    }


//    public void PoolHeart(HeartBalloon heart)
//    {
//        heart.transform.parent = transform;
//        heart.HeartTrigger.enabled = true;

//        //heart.Reset();

//        //heart.StopMoving();

//        StartCoroutine(heart.DeactivateHeart(HeartPoolDelay));
//    }


//    private void OnBalloonPopped(HeartBalloon heart)
//    {
//        PoolHeart(heart);
//    }


//    // initLiftPerc is a percentage of the balloon's MaxLift
//    // altPercent is a percentage of the stickFigure.MaxAltitude
//    public HeartBalloon GetFloatingHeart(float altitude, float initLiftPerc, int beatInterval) //, int bonusValue)
//    {
//        HeartBalloon foundHeart = null;

//        float altPercent = altitude / StickFigure.MaxAltitude * 100f;

//        // first look for a heart in the pool
//        var poolHearts = Pool.Where(x => !x.gameObject.activeSelf && x.MinAltitide <= altPercent && x.MaxAltitide >= altPercent).ToList();

//        // pick a random heart from the pool list
//        if (poolHearts != null && poolHearts.Count > 0)
//            foundHeart = poolHearts[UnityEngine.Random.Range(0, poolHearts.Count)];

//        // no hearts for this altitude in the pool at the moment
//        // instantiate a prefab for the given altitude
//        if (foundHeart == null)  
//        {
//            var altitudePrefabs = HeartPrefabs.Where(x => x.MinAltitide <= altPercent && x.MaxAltitide >= altPercent).ToList();

//            if (altitudePrefabs == null || altitudePrefabs.Count == 0)      // no heart prefabs in altitude range!
//                return null;

//            // pick a random prefab from the list
//            var heartPrefab = altitudePrefabs[UnityEngine.Random.Range(0, altitudePrefabs.Count)];
//            if (heartPrefab != null)
//            {
//                foundHeart = Instantiate(heartPrefab, transform);
//                Pool.Add(foundHeart);
//            }
//        }

//        if (foundHeart != null)
//        {
//            foundHeart.SetInitValues(initLiftPerc, beatInterval); 

//            foundHeart.Pumping = false;
//            foundHeart.Inflating = true;
//            foundHeart.FigureIsPopping = false;

//            //var mat = HeartMaterials.LookupColourMaterial(colour);
//            //foundHeart.SetColour(colour, mat);
//            foundHeart.HeartTrigger.enabled = true;

//            foundHeart.gameObject.SetActive(true);
//        }

//        if (foundHeart == null)
//            Debug.Log($"GetFloatingHeart foundHeart = null!!");

//        return foundHeart;
//    }


//    // get deflating heart (held by figure)
//    public HeartBalloon GetHeldHeart(HeartColour colour, int beatInterval)
//    {
//        var heartPrefab = HeartPrefabs.Where(x => x.HeartColour == colour).FirstOrDefault();
//        if (heartPrefab == null)
//            return null;

//        var heart = Instantiate(heartPrefab, transform);
//        Pool.Add(heart);

//        heart.SetInitValues(0f, beatInterval);

//        heart.Pumping = false;
//        heart.Inflating = false;
//        heart.FigureIsPopping = false;

//        //var mat = HeartMaterials.LookupColourMaterial(colour);
//        //foundHeart.SetColour(colour, mat);
//        heart.HeartTrigger.enabled = true;

//        return heart;
//    }


//    public HeartColour RandomHeartColour(bool excludeGold)
//    {
//        HeartColour randomColour;

//        do
//        {
//            Array array = Enum.GetValues(typeof(HeartColour));
//            randomColour = (HeartColour)array.GetValue(UnityEngine.Random.Range(0, array.Length));
//        }
//        while (excludeGold && randomColour == HeartColour.Gold);

//        return randomColour;
//    }
//}
