﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LeaderboardEntry : MonoBehaviour
{
    public Text Rank;
    public Text SchoolName;
    public Text PlayerName;
    public Text Score;

    public Button LBButton;

    [Header("Audio")]
    public AudioClip FlipAudio;
    public AudioClip FlippedAudio;

    public float FlipVolume = 10f;

    private float flipTime = 0.3f;


    private void OnEnable()
    {
        LBButton.onClick.AddListener(OnLBEntryClick);
    }

    private void OnDisable()
    {
        LBButton.onClick.RemoveListener(OnLBEntryClick);
    }


    public void FlipIn(float initAngle, bool audio)
    {
        //if (audio && FlipAudio != null)
        //    AudioSource.PlayClipAtPoint(FlippedAudio, Vector3.zero);

        if (audio)
            PlaySFX(FlipAudio);

        LeanTween.rotateAround(LBButton.GetComponent<RectTransform>(), Vector3.left, 180f + initAngle, flipTime)
                    .setEaseInCubic()
                    .setOnComplete(() =>
                    {
                        //if (audio)
                            //PlaySFX(FlippedAudio);

                        LeanTween.rotateAround(LBButton.GetComponent<RectTransform>(), Vector3.left, 180f, flipTime)
                                    .setEaseOutCubic()
                                    .setOnComplete(() =>
                                    {
                                        if (audio)
                                            PlaySFX(FlippedAudio);
                                    });
                    }
                    );
    }


    private void OnLBEntryClick()
    {
        //BeatEvents.OnLeaderboardEntryClicked?.Invoke();
    }

    private void PlaySFX(AudioClip sfx)
    {
        if (sfx == null)
            return;

        AudioSource.PlayClipAtPoint(sfx, Vector3.zero, FlipVolume); 

        //LeaderboardAudioSource.clip = sfx;
        //LeaderboardAudioSource.Play();
    }
}
