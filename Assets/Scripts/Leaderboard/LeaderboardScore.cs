﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LeaderboardScore
{
    public string PlayerName;
    public long PlayerScore;

    public string SchoolName;
}
