﻿
using System;
using System.Collections.Generic;
using UnityEngine;


// player name / stats
[CreateAssetMenu(fileName = "PlayerProfile", menuName = "Player/Profile")]
public class PlayerProfile : ScriptableObject
{
	public string PlayerName;              // set on firebase db playerprofile save (and can be set in inspector)
	public string SchoolName;   

	public DateTime DateCreated;
	public DateTime LastPlay;

	//public int PlayCount;

	public bool HasRegisteredName => !String.IsNullOrEmpty(PlayerName);
	public bool HasRegisteredSchool => !String.IsNullOrEmpty(SchoolName);

	public string PlayerSchool => (HasRegisteredName ? PlayerName : "-") + (HasRegisteredSchool ? (" @ " + SchoolName) : "");


	public void OnEnable()
	{
		BeatEvents.OnResetData += OnResetData;
		BeatEvents.OnStartNewGame += OnStartNewGame;
	}

	public void OnDisable()
	{
		BeatEvents.OnResetData -= OnResetData;
		BeatEvents.OnStartNewGame -= OnStartNewGame;
	}

	private void OnResetData()
	{
		PlayerName = "";
		SchoolName = "";

		DateCreated = DateTime.MinValue;
		LastPlay = DateTime.MinValue;
	}

	private void OnStartNewGame()
	{
		LastPlay = DateTime.Now;
	}

	// JSON for leaderboard
	public string ToJson(bool prettyPrint)
	{
		return JsonUtility.ToJson(this, prettyPrint);
	}
}
