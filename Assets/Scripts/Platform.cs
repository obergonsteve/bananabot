﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(MeshRenderer))]
//[RequireComponent(typeof(Rigidbody2D))]

public class Platform : MonoBehaviour
{
	public float RestTimeLimit;         // total rest time in secs
	public float RestTimeWarning;       // eg. start flashing if RestTimeRemaining <= this

	public float RestTimeRemaining;     // total rest time remaining in secs

	public float FlashTime = 0.5f;
	public Color FlashColour = Color.red;
	public AudioClip FlashAudio;

	public float FallGravity = 1f;       // rigidbody gravity scale
	private float MaxDrag = 100f;       // to prevent from moving under impact

	private float PoolDelay = 6f;

	private bool gameInPlay = false;
	private bool figureResting = false;

	private bool flashing = false;
	private bool expired = false;       // rest time used

	private MeshRenderer meshRenderer;
	private Color meshColour;

	private Rigidbody2D rigidBody;
	private Collider2D boxCollider;


	public void OnEnable()
	{
		BeatEvents.OnGameStateChanged += OnGameStateChanged;
		//BeatEvents.OnHeartBeat += OnHeartBeat;
	}

	public void OnDisable()
	{
		BeatEvents.OnGameStateChanged -= OnGameStateChanged;
		//BeatEvents.OnHeartBeat -= OnHeartBeat;
	}


	private void Awake()
	{
		meshRenderer = GetComponent<MeshRenderer>();
		meshColour = meshRenderer.material.color;
		rigidBody = GetComponent<Rigidbody2D>();
		boxCollider = GetComponent<Collider2D>();

		RestTimeRemaining = RestTimeLimit;
		gameInPlay = false;

		Reset();
	}

	private void Update()
	{
		if (!gameInPlay)
			return;

		if (!figureResting)
			return;

		if (expired)
			return;

		RestTimeRemaining -= Time.deltaTime;

		if (RestTimeRemaining <= RestTimeWarning && RestTimeRemaining > 0)
		{
			flashing = true;             // on beat
		}
		else if (RestTimeRemaining <= 0)
		{
			expired = true;
			BeatEvents.OnPlatformExpired?.Invoke(this);

			//Debug.Log($"{name} platform expired!");

			// reset platform
			Reset();

			StartCoroutine(FallUnderGravity());
		}
	}

	private IEnumerator FallUnderGravity()
	{
		rigidBody.bodyType = RigidbodyType2D.Dynamic;
		rigidBody.gravityScale = FallGravity;               // fall under gravity, pool when hit water or another platform
		rigidBody.drag = 0;
		rigidBody.angularDrag = 0;
		boxCollider.enabled = false;

		yield return new WaitForSeconds(PoolDelay);

		//Debug.Log($"{name} OnPlatformRemoved");
		BeatEvents.OnPlatformRemoved?.Invoke(this);     // pools after delay

		yield return null;
	}

	public void Reset()
	{
		flashing = false;
		figureResting = false;
		expired = false;

		meshRenderer.material.color = meshColour;

		rigidBody.gravityScale = 0f;
		rigidBody.drag = MaxDrag;
		rigidBody.angularDrag = MaxDrag;

		boxCollider.enabled = true;

		rigidBody.bodyType = RigidbodyType2D.Kinematic;

		RestTimeRemaining = RestTimeLimit;
	}


	//private void OnHeartBeat(float BPM, long beatTicks, long lastBeatTicks)
	//{
	//	if (flashing)
	//		Flash();
	//}

	private void Flash()
	{
		if (!flashing)
			return;

		LeanTween.color(gameObject, FlashColour, FlashTime)
					.setOnUpdate((Color c) => { meshRenderer.material.color = c; })
					.setEaseOutSine()
					.setOnComplete(() =>
					{
						LeanTween.color(gameObject, meshColour, FlashTime)
									.setOnUpdate((Color c) => { meshRenderer.material.color = c; })
									.setEaseInSine()
									.setOnComplete(() =>
									{
										if (FlashAudio != null)
											AudioSource.PlayClipAtPoint(FlashAudio, transform.position, AudioManager.SFXVolume);
										BeatEvents.OnPlatformFlash?.Invoke(this);
									});
					});
	}

	// true while figure in collision with platform
	public void FigureResting(bool resting)
	{
		figureResting = resting;
	}

	//public void SetColour(Color colour)
	//{
	//	meshRenderer.material.color = colour;
	//}

	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
	{
		gameInPlay = (newState == GameManager.GameState.InPlay);
	}
}
