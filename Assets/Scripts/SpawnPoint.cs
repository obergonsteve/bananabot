﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
	public Fruit FruitPrefab;

	[Space]
	public float MaxFuel = 10f;
	public float MaxHealth = 10f;

	[Header("Time Fruit")]
	public float ClockTime;
	public float TimeFactor = 2f;		// multiplied by remaining time when fruit taken by player

	[Space]
	public Vector2 LaunchForce;

	[Range(0f, 1f)]
	public float GravityScale = 0f;

	[Header("Spawning")]
	public bool RandomSpawn = true;     // randomly chosen points at random spawn intervals

	[Header("Random spawning")]
	public float MinRandomDelay = 0.5f;       // seconds to first spawn
	public float MaxRandomDelay = 5f;       // seconds to first spawn
	public float MinRandomInterval = 1f;    // seconds between spawns
	public float MaxRandomInterval = 10f;    // seconds between spawns
	public int MinRandomQuantity = 1;       // qty of prefab to spawn each time
	public int MaxRandomQuantity = 3;       // qty of prefab to spawn each time

	private float RandomSpawnDelay => UnityEngine.Random.Range(MinRandomDelay, MaxRandomDelay);
	private float RandomSpawnInterval => UnityEngine.Random.Range(MinRandomInterval, MaxRandomInterval);
	private float RandomSpawnQuantity => UnityEngine.Random.Range(MinRandomQuantity, MaxRandomQuantity);

	[Header("Non-random spawning")]
	public float SpawnDelay = 1f;		// seconds to first spawn
	public float SpawnInterval = 1f;    // seconds between spawns
	public int SpawnQuantity = 1;       // qty of prefab to spawn each time

	private bool spawning = false;


	private void OnEnable()
	{
        BeatEvents.OnNewGame += OnNewGame;
        BeatEvents.OnGameStateChanged += OnGameStateChanged;
	}

    private void OnDisable()
    {
        BeatEvents.OnNewGame -= OnNewGame;
		BeatEvents.OnGameStateChanged -= OnGameStateChanged;
	}

	private void OnNewGame(GameScores scores)
	{
		//if (! RandomSpawn)       // spawned via FruitSpawner
			StartCoroutine(StartSpawning());
	}

	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
	{
		if (newState == GameManager.GameState.GameOver)
			spawning = false;
	}

	private IEnumerator StartSpawning()
	{
		if (spawning)
			yield break;

		spawning = true;

		while (spawning)
		{
			yield return new WaitForSeconds(RandomSpawn ? RandomSpawnDelay : SpawnDelay);

			var spawnQty = RandomSpawn ? RandomSpawnQuantity : SpawnQuantity;

			for (int i = 0; i < spawnQty; i++)
			{
				SpawnFruit();
			}

			yield return new WaitForSeconds(RandomSpawn ? RandomSpawnInterval : SpawnInterval);
		}

		yield return null;
	}

	public void SpawnFruit()
	{
		var newFruit = Instantiate(FruitPrefab, transform);
		newFruit.Spawn(this);
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, 1f);
	}
}
