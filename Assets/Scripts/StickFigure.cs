﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StickFigure : MonoBehaviour
{
	public Animator animator;
	public Rigidbody2D rigidBody;

	public Transform Figure;
	public ParticleSystem Jetpack;

	public AudioClip JetpackAudio;

	public GameSettings gameSettings;

	public float HealthLevel;		// 0-100
	public float FuelLevel;         // 0-100

	public const float MaxHealthFuel = 100f;
	public float ThrusterFuelRate;
	public float ThrowFuelRate;

	private float ThrowDelay = 0.25f;				// if player not flipping before throwing

	private float flipTime = 0.5f;                    // face left / right
	private bool flipping = false;					// to face opposite direction

	[Header("Looking Down")]
	public float PlatformReach;						// raycast to platform (eg. for flapping animation)
	public Transform RayCastOrigin;						// raycast to platform (eg. for flapping animation)
	public LayerMask platformLayer;				// max raycast to platform
	public LayerMask groundLayer;                   // raycast to ground
	public LayerMask obstacleLayer;                   // raycast to obstacle

	private float sphereCastRadius = 0.6f;          // for raycast downwards

	public Fruit ThrowFruitPrefab;
	public Transform ThrowOrigin;                

	private bool lookingDown = false;
	private float lookDownInterval = 0.025f;      // seconds

	public float AirDrag = 0.5f;
	//public float WaterDrag = 5f;

	private float landingDownForce = 2f;        // to help ensure collision for landing

	// direction
	//private Vector3 lastPosition = Vector3.zero;

	public bool MovingHoriz => Mathf.Abs(rigidBody.velocity.x) > 0f;
	public bool MovingLeft => rigidBody.velocity.x < 0f;

	public bool MovingVertically => Mathf.Abs(rigidBody.velocity.y) > 0f;
	public bool MovingUp => rigidBody.velocity.y > 0.05f;

	private bool justLanded = false;		// reset to false on AddForce

	// animation
	private const string FloatTrigger = "Float";
	private const string FallTrigger = "Fall";
	private const string FlapTrigger = "Flap";
	private const string LandingTrigger = "Land";
	private const string SplashTrigger = "Splash";
	private const string IdleTrigger = "Idle";
	private const string ThrowTrigger = "Throw";

	private bool animationChecking = false;
	private float animationCheckInterval = 0.05f;      // seconds

	private Platform platformBelow = null;
	private Obstacle obstacleBelow = null;
	private Ground groundBelow = null;


	public enum State
	{
		None,     
		Standing,		// idle - after land animation
		Floating,		// upwards movement
		Flapping,       // from floating / idle
		Falling,        // 'seen' platform / water below (clamber)
		Landing,		// on collision with platform (if falling)
		InWater,		// on collision with water	
	}
	[Space]
	public State CurrentState;

	public enum Facing
	{
		Left,
		Right
	}
	private Facing currentFacing;


	private void OnEnable()
	{
		BeatEvents.OnNewGame += OnNewGame;	
		BeatEvents.OnStartPlay += OnStartPlay;
		//BeatEvents.OnGameRestart += OnGameRestart;	
		BeatEvents.OnSwipeEnd += OnSwipeEnd;			// add force
		BeatEvents.OnFigureLanded += OnFigureLanded;	
		BeatEvents.OnPlayerTakeFruit += OnPlayerTakeFruit;
		BeatEvents.OnPlatformExpired += OnPlatformExpired;
	}

	private void OnDisable()
	{
		BeatEvents.OnNewGame -= OnNewGame;
		BeatEvents.OnStartPlay -= OnStartPlay;
		//BeatEvents.OnGameRestart -= OnGameRestart;
		BeatEvents.OnSwipeEnd -= OnSwipeEnd;
		BeatEvents.OnFigureLanded -= OnFigureLanded;
		BeatEvents.OnPlayerTakeFruit -= OnPlayerTakeFruit;
		BeatEvents.OnPlatformExpired -= OnPlatformExpired;
	}


	private void Start()
	{
		SetState(State.Standing);
		Stop();
		//swipeBoost = true;
		rigidBody.drag = AirDrag;
	}

	private void OnNewGame(GameScores scores)
	{
		Land();
	}

	private void OnStartPlay()
	{
		HealthLevel = MaxHealthFuel;
		FuelLevel = MaxHealthFuel;

		BeatEvents.OnPlayerHealthFuelChanged?.Invoke(0, HealthLevel, 0, FuelLevel);

		StartCoroutine(AnimationState());
	}

	// land on platform
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Fruit"))
		{
			//Debug.Log($"Platform Collision ENTER: CurrentState = {CurrentState} platformBelow = {platformBelow}");
			var fruit = collision.gameObject.GetComponent<Fruit>();
			BeatEvents.OnFigureHitFruit?.Invoke(fruit);
		}
		else if (collision.gameObject.CompareTag("Ground"))
		{
			//Debug.Log($"Platform Collision ENTER: CurrentState = {CurrentState} platformBelow = {platformBelow}");
			var ground = collision.gameObject.GetComponent<Ground>();

			// only 'land' if ground below (from raycast)
			if (ground != null && (CurrentState == State.Flapping || CurrentState == State.Falling))
			{
				SetState(State.Landing);
			}

			BeatEvents.OnFigureHitGround?.Invoke(ground);
		}
		else if (collision.gameObject.CompareTag("Platform"))
		{
			//Debug.Log($"Platform Collision ENTER: CurrentState = {CurrentState} platformBelow = {platformBelow}");
			var platform = collision.gameObject.GetComponent<Platform>();

			// only 'land' if platform below (from raycast)
			if (platform != null && (CurrentState == State.Flapping || CurrentState == State.Falling) && platformBelow == platform)	
			{
				SetState(State.Landing);
				platform.FigureResting(true);
			}

			BeatEvents.OnFigureHitPlatform?.Invoke(platform);
		}
		else if (collision.gameObject.CompareTag("Obstacle"))
		{
			//Debug.Log($"Platform Collision ENTER: CurrentState = {CurrentState} platformBelow = {platformBelow}");
			var obstacle = collision.gameObject.GetComponent<Obstacle>();

			// only 'land' if platform below (from raycast)
			if (obstacle != null && (CurrentState == State.Flapping || CurrentState == State.Falling) && obstacleBelow == obstacle)
			{
				SetState(State.Landing);
			}

			BeatEvents.OnFigureHitObstacle?.Invoke(obstacle);
		}
	}

	private void OnCollisionExit2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Platform"))
		{
			//Debug.Log($"Platform Collision EXIT: CurrentState = {CurrentState}");

			var platform = collision.gameObject.GetComponent<Platform>();
			if (platform != null)
			{
				platform.FigureResting(false);

				//if (CurrentState == State.Landing || CurrentState == State.Standing)
				//{
				//	SetState(State.Flapping);
				//}
			}
		}
	}

	// add physics impulse to figure according to swipe speed and direction
	// validSwipe is false if 'spamming' (ie. excessive swipes per beat)
	private void OnSwipeEnd(Vector2 endPosition, Vector2 direction, float speed, float swipeForce, int touchCount)
	{
		float impulse = speed * swipeForce;

		if (impulse > 0)
		{
			if (touchCount == 1)        // jetpack boost
			{
				//Debug.Log($"OnSwipeEnd: AddForce {direction * impulse}");
				rigidBody.AddForce(direction * impulse, ForceMode2D.Impulse);
				justLanded = false;

				Jetpack.Play();
				if (JetpackAudio != null)
					AudioSource.PlayClipAtPoint(JetpackAudio, transform.position, AudioManager.SFXVolume);

				BeatEvents.OnFigureImpulse?.Invoke(direction, impulse);

				// use fuel
				var fuelUsed = impulse * ThrusterFuelRate;
				FuelLevel -= fuelUsed;
				FuelLevel = Mathf.Clamp(FuelLevel, 0f, MaxHealthFuel);
				BeatEvents.OnPlayerHealthFuelChanged?.Invoke(0, HealthLevel, fuelUsed, FuelLevel);

				if (FuelLevel <= 0)
					BeatEvents.OnZeroFuel?.Invoke();
			}
			else if (touchCount == 2)        // throw
			{
				if (direction.x < 0)
					StartCoroutine(ThrowFruit(Facing.Left, direction, impulse));
				else
					StartCoroutine(ThrowFruit(Facing.Right, direction, impulse));

				// use fuel
				var fuelUsed = impulse * ThrowFuelRate;
				FuelLevel -= fuelUsed;
				FuelLevel = Mathf.Clamp(FuelLevel, 0f, MaxHealthFuel);
				BeatEvents.OnPlayerHealthFuelChanged?.Invoke(0, HealthLevel, fuelUsed, FuelLevel);

				if (FuelLevel <= 0)
					BeatEvents.OnZeroFuel?.Invoke();
			}
		}
	}

	// increase player health if fruit not fully rotten, otherwise reduce health
	// increase player fuel level according to fruit fuel level
	private void OnPlayerTakeFruit(float fruitFuel, float fruitHealth, bool isRotten, float fruitToxicity, float secondsRemaining)
	{
		//Debug.Log($"OnPlayerTakeFruit: fruitFuel = {fruitFuel}, fruitHealth = {fruitHealth}, fruitToxicity = {fruitToxicity}");

		float healthChange = 0f;
		; 
		// fruit reduces health once it is totally rotten
		if (isRotten)
			healthChange = -fruitToxicity;
		else		// otherwise it increases it
			healthChange = fruitHealth;

		HealthLevel += healthChange;

		HealthLevel = Mathf.Clamp(HealthLevel, 0f, MaxHealthFuel);

		if (HealthLevel <= 0)
			BeatEvents.OnZeroHealth?.Invoke();

		FuelLevel += fruitFuel;
		FuelLevel = Mathf.Clamp(FuelLevel, 0f, MaxHealthFuel);

		BeatEvents.OnPlayerHealthFuelChanged?.Invoke(healthChange, HealthLevel, fruitFuel, FuelLevel);
	}

	// end of Landing animation event
	private void OnFigureLanded()
	{
		if (CurrentState == State.Landing)
		{
			Land();
		}
	}

	private void Land()
	{
		Stop();
		SetState(State.Standing);
		justLanded = true;
	}

	private void OnPlatformExpired(Platform platform)
	{
		if (CurrentState == State.Standing)
		{
			SetState(State.Floating);		// TODO: was Flapping 
		}
	}


	private void SetState(State newState)
	{
		if (newState == CurrentState)
			return;

		BeatEvents.OnFigureStateChanged?.Invoke(CurrentState, newState);
		CurrentState = newState;

		switch (CurrentState)
		{
			case State.Floating:
				animator.SetTrigger(FloatTrigger);
				break;
			case State.Flapping:
				animator.SetTrigger(FlapTrigger);
				break;
			case State.Falling:
				animator.SetTrigger(FallTrigger);
				AddDownForce();
				break;
			case State.Landing:
				animator.SetTrigger(LandingTrigger);
				AddDownForce();
				break;
			case State.InWater:
				animator.SetTrigger(SplashTrigger);
				break;
			case State.Standing:
				animator.SetTrigger(IdleTrigger);
				break;
			case State.None:
				break;
		}

		//Debug.Log($"SetState: {CurrentState}");
	}

	private void Stop()
	{
		rigidBody.velocity = Vector3.zero;
		//Debug.Log($"Stop rigidBody.gravityScale = {rigidBody.gravityScale}");
	}


	private void AddDownForce()
	{
		//Debug.Log("AddDownForce");
		rigidBody.AddForce(Vector3.down * landingDownForce, ForceMode2D.Impulse);
	}


	private void SetFacing(Facing facing)
	{
		if (currentFacing == facing)
			return;

		if (flipping)
			return;

		currentFacing = facing;
		flipping = true;

		BeatEvents.OnFigureFlip?.Invoke(currentFacing);

		LeanTween.rotateAround(Figure.gameObject, Figure.transform.up, 180f, flipTime)
					.setEaseOutBack()
					.setOnComplete(() => { flipping = false; } );
	}

	// quick flip (if required) and throw new fruit
	private IEnumerator ThrowFruit(Facing facing, Vector2 direction, float force)
	{
		if (flipping)
			yield break;

		animator.SetTrigger(ThrowTrigger);

		// flip if not facing direction of swipe
		if (currentFacing != facing)
		{
			currentFacing = facing;
			flipping = true;

			BeatEvents.OnFigureFlip?.Invoke(currentFacing);

			LeanTween.rotateAround(Figure.gameObject, Figure.transform.up, 180f, flipTime * 0.5f)
						.setEaseOutBack()
						.setOnComplete(() =>
						{
							flipping = false;
							BeatEvents.OnThrowFruit?.Invoke(ThrowFruitPrefab, ThrowOrigin.position, direction, force);
						});
		}
		else
		{
			yield return new WaitForSeconds(ThrowDelay);
			BeatEvents.OnThrowFruit?.Invoke(ThrowFruitPrefab, ThrowOrigin.position, direction, force);
		}

		yield return null;
	}


	private IEnumerator AnimationState()
	{
		if (animationChecking)
			yield break;

		animationChecking = true;

		while (animationChecking)
		{
			//if (! justLanded)
			{
				if (MovingVertically)
				{
					if (MovingUp)
					{
						SetState(State.Floating);		// from any state
					}
					else		// moving downwards
					{
						platformBelow = null;
						obstacleBelow = null;
						groundBelow = null;

						if (!LookDownForPlatform())
						{
							if (!LookDownForObstacle())
							{
								LookDownForGround();
							}
						}

						if (platformBelow != null || obstacleBelow != null || groundBelow != null)
						{
							if (CurrentState == State.Floating || CurrentState == State.Flapping)
							{
								SetState(State.Falling);
							}
						}
						else    // moving down and nothing below!
						{
							SetState(State.Flapping);		// from any state
						}
					}
				}

				if (!justLanded && MovingHoriz)
				{
					if (MovingLeft)
						SetFacing(Facing.Left);
					else
						SetFacing(Facing.Right);
				}
			}

			yield return new WaitForSeconds(lookDownInterval);
		}

		yield return null;
	}

	private bool LookDownForPlatform()
	{
		// raycast to 'see' platform below
		RaycastHit2D platformRaycastHit = Physics2D.CircleCast(RayCastOrigin.position, sphereCastRadius, -transform.up, PlatformReach, platformLayer);

		if (platformRaycastHit.collider != null)         // platform just below!
		{
			platformBelow = platformRaycastHit.collider.GetComponent<Platform>();
			return true;
		}
		return false;
	}

	private bool LookDownForObstacle()
	{
		// raycast to 'see' obstacle below
		RaycastHit2D obstacleRaycastHit = Physics2D.CircleCast(RayCastOrigin.position, sphereCastRadius, -transform.up, PlatformReach, obstacleLayer);

		if (obstacleRaycastHit.collider != null)         // obstacle just below!
		{
			obstacleBelow = obstacleRaycastHit.collider.GetComponent<Obstacle>();
			return true;
		}
		return false;
	}

	private bool LookDownForGround()
	{
		// raycast to 'see' ground below
		RaycastHit2D groundRaycastHit = Physics2D.CircleCast(RayCastOrigin.position, sphereCastRadius, -transform.up, PlatformReach, groundLayer);

		if (groundRaycastHit.collider != null)         // ground just below!
		{
			groundBelow = groundRaycastHit.collider.GetComponent<Ground>();
			return true;
		}
		return false;
	}

	private void OnDrawGizmos()
	{
		// raycast
		Gizmos.color = Color.magenta;
		Gizmos.DrawWireSphere(RayCastOrigin.position - transform.up * PlatformReach, sphereCastRadius);
	}
}
