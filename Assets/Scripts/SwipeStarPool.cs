﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeStarPool : MonoBehaviour
{
    public List<SwipeStars> Pool;
    public SwipeStars SwipeStarPrefab;

    public float PoolDelay;           // to allow particles to play out


    public void PoolSwipeStars(SwipeStars swipeStars)
    {
        StartCoroutine(swipeStars.DeactivateStars(PoolDelay));
    }


    public SwipeStars GetSwipeStars()
    {
        SwipeStars foundStars = null;

        foreach (var stars in Pool)
        {
            if (! stars.gameObject.activeSelf)
            {
				stars.gameObject.SetActive(true);
				foundStars = stars;
                break;
            }
        }

        if (foundStars == null)
        {
            foundStars = Instantiate(SwipeStarPrefab, transform);
            Pool.Add(foundStars);
        }

        return foundStars;
    }
}
