﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Handle screen touches
// Mouse clicks emulate touches
// Keeps track of beat ticks for touch timing
public class SwipeStars : MonoBehaviour
{
    public List<ParticleSystem> Particles;
    public ParticleSystem FinalParticles;

    private Camera mainCamera;

	private void Awake()
	{
        mainCamera = Camera.main;
    }

	public void SwipeStart(Vector2 startPosition)
    {
        //Debug.Log("SwipeStart");
        PlayFinalParticles(startPosition);
        PlayStars(startPosition);
    }

    public void SwipeMove(Vector2 movePosition)
    {
        //Debug.Log("SwipeMove");
        MoveParticles(movePosition);
    }

    public void SwipeEnd(Vector2 endPosition)
    {
        //Debug.Log("SwipeEnd");
        StopStars();
        PlayFinalParticles(endPosition);
    }


    private void PlayFinalParticles(Vector2 touchPosition)
    {
        FinalParticles.transform.position = mainCamera.ScreenToWorldPoint(touchPosition);
        FinalParticles.Play();
    }

    private void PlayStars(Vector2 touchPosition)
    {
        MoveParticles(touchPosition);

        foreach (var particles in Particles)
        {
            if (! particles.isPlaying)
                particles.Play();           // looping
        }
    }

    private void MoveParticles(Vector2 touchPosition)
    {
        Vector2 worldPosition = mainCamera.ScreenToWorldPoint(touchPosition);

        foreach (var particles in Particles)
        {
            particles.transform.position = worldPosition;
        }
    }

    private void StopStars()
    {
        foreach (var particles in Particles)
        {
            if (particles.isPlaying)
                particles.Stop();           // looping
        }
    }

    public IEnumerator DeactivateStars(float poolDelay)
    {
        yield return new WaitForSeconds(poolDelay);

        gameObject.SetActive(false);      // ie pooled
        StopStars();

        yield return null;
    }
}
