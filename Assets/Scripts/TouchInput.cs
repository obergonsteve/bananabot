﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Handle screen touches
// Mouse clicks emulate touches
// Keeps track of beat ticks for touch timing
public class TouchInput : MonoBehaviour
{
    public SwipeStarPool SwipeStarPool;
    public float SwipeForce;		    // increase to apply to more force on swipe

	private Vector2 startPosition;
    private Vector2 moveDirection;
    private Vector2 endPosition;

    private long touchStartTicks; 
    private long touchEndTicks;

    private int tapCount = 0;

	private SwipeStars swipeStars;

    private GameManager.GameState gameState = GameManager.GameState.NotStarted;

	private bool OptionsShowing;


	public void OnEnable()
    {
		BeatEvents.OnGameStateChanged += OnGameStateChanged;
        BeatEvents.OnGameOptions += OnGameOptions;
    }

	public void OnDisable()
    {
		BeatEvents.OnGameStateChanged -= OnGameStateChanged;
		BeatEvents.OnGameOptions -= OnGameOptions;
	}

	private void OnGameStateChanged(GameManager.GameState currentState, GameManager.GameState newState)
    {
        gameState = newState;

        if (gameState == GameManager.GameState.GameOver)
            tapCount = 0;       // TODO: ??
    }


	private void OnGameOptions(bool showing)
	{
		OptionsShowing = showing;
	}


	private void Update()
	{
		if (OptionsShowing)
		{
			return;
		}

		// track a single touch
		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);

			// handle finger movements based on TouchPhase
			switch (touch.phase)
			{
				case TouchPhase.Began:
					StartTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Moved:
					MoveTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Ended:
					EndTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Stationary:         // touching but hasn't moved
					break;

				case TouchPhase.Canceled:           // system cancelled touch
					break;
			}
		}

		// emulate touch with left mouse
		else if (Input.GetMouseButtonDown(0))
		{
			StartTouch(Input.mousePosition, 1);
		}
		else if (Input.GetMouseButton(0))
		{
			MoveTouch(Input.mousePosition, 1);
		}
		else if (Input.GetMouseButtonUp(0))
		{
			EndTouch(Input.mousePosition, 1);
		}

		// right mouse == 2 finger swipe
		else if (Input.GetMouseButtonDown(1))
		{
			StartTouch(Input.mousePosition, 2);
		}
		else if (Input.GetMouseButton(1))
		{
			MoveTouch(Input.mousePosition, 2);
		}
		else if (Input.GetMouseButtonUp(1))
		{
			EndTouch(Input.mousePosition, 2);
		}
	}

	private void StartTouch(Vector2 position, int touchCount)
	{
		if (touchCount == 1)
		{
			tapCount++;
			BeatEvents.OnPlayerTap?.Invoke(gameState, tapCount);        // capture all taps for GameManager
		}

		if (gameState == GameManager.GameState.NotStarted || gameState == GameManager.GameState.ReadyToStart
						|| gameState == GameManager.GameState.Paused || gameState == GameManager.GameState.GameOver)
			return;

		startPosition = position;
		touchStartTicks = DateTime.Now.Ticks;

		swipeStars = SwipeStarPool.GetSwipeStars();
		swipeStars.SwipeStart(startPosition);

		BeatEvents.OnSwipeStart?.Invoke(startPosition, touchCount);
	}

	private void MoveTouch(Vector2 position, int touchCount)
	{
		Vector2 movePosition = position;

		if (movePosition != startPosition)
		{
			BeatEvents.OnSwipeMove?.Invoke(movePosition, touchCount);

			if (swipeStars != null)
				swipeStars.SwipeMove(movePosition);
		}
	}

	private void EndTouch(Vector2 position, int touchCount)
	{
		endPosition = position;
		touchEndTicks = DateTime.Now.Ticks;
		moveDirection = (endPosition - startPosition).normalized;

		float moveDistance = Vector2.Distance(startPosition, endPosition);
		float moveSpeed = 0;     // distance / time

		if (touchEndTicks - touchStartTicks > 0)            // should be!!
			moveSpeed = moveDistance / (touchEndTicks - touchStartTicks);     // distance / time

		// beat timing
		if (moveSpeed > 0)
		{
			//Debug.Log($"OnSwipeEnd: touchStartBeatAccuracy = {touchStartBeatAccuracy} touchEndBeatAccuracy = {touchEndBeatAccuracy}");
			BeatEvents.OnSwipeEnd?.Invoke(endPosition, moveDirection, moveSpeed, SwipeForce, touchCount);
		}

		if (swipeStars != null)
		{
			swipeStars.SwipeEnd(endPosition);
			SwipeStarPool.PoolSwipeStars(swipeStars);       // after delay
			swipeStars = null;
		}
	}
}
