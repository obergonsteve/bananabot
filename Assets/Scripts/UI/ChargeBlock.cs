﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChargeBlock : MonoBehaviour
{
    public Image EmptyBlock;
    public Image FullBlock;

    private float PulseTime = 0.1f;
    private float PulseScale = 1.4f;

    public float EmptyTransparency;

    private bool pulsing = false;
    public bool IsFull => FullBlock.enabled;      // empty block on top of full block


    public void OnEnable()
    {

    }

    public void OnDisable()
    {

    }


    public void SetChargeState(bool full, bool pulse)
    {
        if (pulse)
            Pulse();

        FullBlock.enabled = full;
        EmptyBlock.enabled = !full;
    }


    private void Pulse() //float BPM)
    {
        if (pulsing)
            return;

        var outScale = Vector3.one * PulseScale;
        var inScale = Vector3.one; // transform.localScale;

        pulsing = true;

        LeanTween.scale(gameObject, outScale, PulseTime)
                    .setEaseInBack()
                    .setOnComplete(() =>
                    {
                        LeanTween.scale(gameObject, inScale, PulseTime)
                                    .setEaseOutBack()
                                    .setOnComplete(() => { pulsing = false; });
                    });
    }

    public void SetColour(Color colour)
    {
        FullBlock.color = colour;
        EmptyBlock.color = new Color(colour.r, colour.g, colour.b, EmptyTransparency);
    }
}
