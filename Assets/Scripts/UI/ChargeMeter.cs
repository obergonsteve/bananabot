﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChargeMeter : MonoBehaviour
{
    public ChargeBlock ChargeBlockPrefab;
    public Gradient ChargeColourGradient;

    public AudioClip ChargeUpAudio;

    public int NumChargeBlocks;                // max charge divided into blocks

    private List<ChargeBlock> chargeBlocks = new List<ChargeBlock>();
    private int fullBlockCount = -1;        // to detect change in charge blocks

    private float chargeInterval = 0.05f;


    private void InitBlocks()
    {
        chargeBlocks.Clear();

        foreach (Transform block in transform)
        {
            Destroy(block.gameObject);
        }

        for (int i = 0; i < NumChargeBlocks; i++)
        {
            var newBlock = Instantiate(ChargeBlockPrefab, transform);
            chargeBlocks.Add(newBlock);

            newBlock.SetChargeState(false, false);
        }

        fullBlockCount = -1;
    }

    public IEnumerator SetCharge(float newCharge, float maxCharge)
    {
        float chargePerBlock = maxCharge / NumChargeBlocks;
        int fullChargeBlocks = (int)(newCharge / chargePerBlock);

        if (newCharge == 0)
            InitBlocks();

        if (fullChargeBlocks == fullBlockCount)         // no change in blocks
            yield break;

        fullBlockCount = fullChargeBlocks;

        EmptyAll();

        if (newCharge > 0)
        {
            if (ChargeUpAudio != null)
                AudioSource.PlayClipAtPoint(ChargeUpAudio, Vector3.zero, AudioManager.SFXVolume);

            BeatEvents.OnLifeChargeIncrement?.Invoke(newCharge, fullChargeBlocks);      // electricity particles
        }

        for (int i = 0; i < chargeBlocks.Count; i++)
        {
            bool full = i < fullChargeBlocks;

            // initialise block colours according to charge value
            if (newCharge == 0)
            {
                chargeBlocks[i].SetColour(ChargeColourGradient.Evaluate(chargePerBlock * (i + 1) / maxCharge));
                fullBlockCount = 0;
            }

            chargeBlocks[i].SetChargeState(full, full);

            if (full)
                yield return new WaitForSeconds(chargeInterval);
            else
                yield return null;
        }
    }

    //  public IEnumerator SetCharge(float newCharge, float maxCharge)
    //  {
    //      float chargePerBlock = maxCharge / NumChargeBlocks;
    //      int fullChargeBlocks = (int)(newCharge / chargePerBlock);

    ////bool fullBlocksChanged = false;

    //if (newCharge == 0)
    //          InitBlocks();

    //      EmptyAll();

    //      for (int i = 0; i < chargeBlocks.Count; i++)
    //      {
    //          bool full = i < fullChargeBlocks;

    //          // initialise block colours according to charge value
    //          if (newCharge == 0)
    //          {
    //              chargeBlocks[i].SetColour(ChargeColourGradient.Evaluate(chargePerBlock * (i + 1) / maxCharge));
    //              fullBlockCount = 0;
    //          }
    //	else if (fullChargeBlocks != fullBlockCount)            // full blocks changed - play audio
    //	{
    //		fullBlockCount = fullChargeBlocks;
    //		//fullBlocksChanged = true;

    //		if (ChargeUpAudio != null)
    //			AudioSource.PlayClipAtPoint(ChargeUpAudio, Vector3.zero, 2f);
    //	}

    //	chargeBlocks[i].SetChargeState(full, false); // full && fullBlocksChanged);

    //          if (full && newCharge > 0)
    //          {
    //		//if (ChargeUpAudio != null)
    //		//    AudioSource.PlayClipAtPoint(ChargeUpAudio, Vector3.zero, 2f);

    //		yield return new WaitForSeconds(chargePause);
    //          }
    //          else
    //              yield return null;
    //      }
    //  }

    private void EmptyAll()
    {
        foreach (var block in chargeBlocks)
        {
            block.SetChargeState(false, false);
        }
    }
}
