﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class LifeCharge : MonoBehaviour
//{
//    public float CurrentCharge;
//    public float MaxCharge;

//	public ParticleSystem Electricity;

//	//public AudioClip ChargeUpAudio;
//	public AudioClip DischargeAudio;

//	public void OnEnable()
//	{
//		BeatEvents.OnLifeChargeIncrease += OnLifeChargeIncrease;
//		BeatEvents.OnLifeChargeDischarge += OnLifeChargeDischarge;
//		BeatEvents.OnLifeChargeIncrement += OnLifeChargeIncrement;
//		BeatEvents.OnNewGame += OnNewGame;
//	}

//	public void OnDisable()
//	{
//		BeatEvents.OnLifeChargeIncrease -= OnLifeChargeIncrease;
//		BeatEvents.OnLifeChargeDischarge -= OnLifeChargeDischarge;
//		BeatEvents.OnLifeChargeIncrement -= OnLifeChargeIncrement;
//		BeatEvents.OnNewGame -= OnNewGame;
//	}

//	private void OnLifeChargeIncrease(float increase)
//	{
//		//Debug.Log("OnLifeChargeIncrease: increase = " + increase + " ChargeValue = " + CurrentCharge);
//		var prevCharge = CurrentCharge;
//		CurrentCharge += increase;

//		if (CurrentCharge >= MaxCharge)
//		{
//			BeatEvents.OnLifeChargeDischarge?.Invoke(true);
//			CurrentCharge = 0;
//		}

//		//ChargeAudio(prevCharge, CurrentCharge);
//		BeatEvents.OnLifeChargeChanged?.Invoke(prevCharge, CurrentCharge, MaxCharge);
//	}


//	private void OnLifeChargeDischarge(bool saveLife)
//	{
//		Electricity.Play();

//		if (DischargeAudio != null)
//			AudioSource.PlayClipAtPoint(DischargeAudio, transform.position, AudioManager.SFXVolume);
//	}

//	private void OnLifeChargeIncrement(float newCharge, int chargeBlocks)
//	{
//		Electricity.Play();
//	}

//	private void OnNewGame(GameScores scores)
//	{
//		BeatEvents.OnLifeChargeChanged?.Invoke(CurrentCharge, 0, MaxCharge);
//		CurrentCharge = 0;
//	}


//	//private void ChargeAudio(float prevCharge, float newCharge)
//	//{
//	//	if (newCharge > prevCharge)
//	//	{
//	//		if (ChargeUpAudio != null)
//	//			AudioSource.PlayClipAtPoint(ChargeUpAudio, transform.position);
//	//	}
//	//}
//}
