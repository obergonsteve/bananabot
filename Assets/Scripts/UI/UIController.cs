﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	public GameSettings GameSettings;

	public Image StoryPanel;

	public Text TapToPlay;
	public Text SwipeToMove;

	public Image GameLogo;
	public Image CreditsPanel;

	[Header("Options")]
	public Button OptionsButton;
	public CanvasGroup OptionsPanel;

	public Slider MusicVolumeSlider;
	public Slider DrumsVolumeSlider;
	public Slider SFXVolumeSlider;

	public Button LeaderboardButton;

	public Button RestartButton;
	public Button ConfirmRestartButton;
	public Button ResetButton;					// reset data (scores, player profile)
	public Button ConfirmResetButton;					// reset data (scores, player profile)
	public Button QuitButton;
	public Button ConfirmQuitButton;

	public float ConfirmTimeout = 3f;

	private float optionsScaleTime = 0.75f;
	private bool optionsPanelShowing = false;

	[Header("Scores")]
	public Image RunningScorePanel;
	public Text RunningScore;
	public Text PBScore;

	[Header("Game Over")]
	public Image GameOverPanel;
	public Text GameScores;
	public Text PBScores;
	public Text NewPersonalBest;
	public Button PlayAgainButton;
	public Button StatsButton;

	public Text ZeroFuel;
	public Text ZeroHealth;
	public Text ZeroTime;

	public Text FinalScore;
	public Text StatsButtonText;
	public CanvasGroup StatsPanel;

	private Coroutine flashNewPBCoroutine;
	private bool newPBFlashing = false;
	private float newPBFlashTime = 0.6f;		// ie. 100 BPM

	public BonusUI BonusUIPrefab;
	public Transform BonusScores;

	private float scoreScaleTime = 1f;
	private bool hilightScore = false;			// extra pulse (eg. on bonus)
	private float scoreHilightScaleFactor = 1.75f;

	[Header("Leaderboard")]
	public LeaderboardUI LeaderboardUI;
	private bool leaderboardPanelShowing = false;

	[Header("Meters")]
	public Image MetersPanel;
	public Slider HealthMeter;
	public Slider FuelMeter;
	public Slider TimeRemainingMeter;

	public float MeterUpdateTime = 0.5f;

	[Header("Fade to Black")]
	public Image Blackout;
	public Color BlackOutColour;
	public Color WhiteOutColour;

	private float fadeToBlackTime = 0.5f;
	private float fadeToWhiteTime = 0.5f;

	private float PulseScale = 1.15f;            // eg. slider, score
	private float PulseTime = 0.25f;            // eg. slider

	private const float NewPBPulseScale = 1.5f;    
	private const float NewPBPulseTime = 0.5f;   

	private int playCount;
	private int SwipeToBeatPlayCount = 3;           // 'swipe to beat' message for first 3 games

	private bool gameOver = false;


	public void OnEnable()
	{
		BeatEvents.OnNewGame += OnNewGame;
		BeatEvents.OnNewGameStarted += OnRestartGame;
		BeatEvents.OnStartPlay += OnStartPlay;
		BeatEvents.OnPlayerTap += OnPlayerTap;

		BeatEvents.OnUpdateScore += OnUpdateScore;
		BeatEvents.OnScoreReset += OnScoreReset;
		BeatEvents.OnFinalGameScore += OnFinalGameScore;
		BeatEvents.OnNewPersonalBest += OnNewPersonalBest;
		BeatEvents.OnBonusScoreFeedback += OnBonusScoreFeedback;
		BeatEvents.OnBonusScoreIncrement += OnBonusScoreIncrement;
		BeatEvents.OnToggleLeaderboard += ToggleLeaderboardsPanel;
		BeatEvents.OnFigureStateChanged += OnFigureStateChanged;

		BeatEvents.OnZeroHealth += OnZeroHealth;
		BeatEvents.OnZeroFuel += OnZeroFuel;
		BeatEvents.OnZeroTime += OnZeroTime;
		BeatEvents.OnPlayerHealthFuelChanged += OnPlayerHealthFuel;
		BeatEvents.OnTimeRemainingChanged += OnTimeRemainingChanged;

		OptionsButton.onClick.AddListener(ToggleOptionsPanel);
		LeaderboardButton.onClick.AddListener(OnLeaderboardButton);
		RestartButton.onClick.AddListener(OnRestartButton);
		ResetButton.onClick.AddListener(OnResetButton);
		QuitButton.onClick.AddListener(OnQuitButton);

		PlayAgainButton.onClick.AddListener(OnPlayAgain);
		StatsButton.onClick.AddListener(OnToggleStats);

		ConfirmResetButton.onClick.AddListener(ConfirmResetClicked);
		ConfirmRestartButton.onClick.AddListener(ConfirmRestartClicked);
		ConfirmQuitButton.onClick.AddListener(ConfirmQuitClicked);

		MusicVolumeSlider.onValueChanged.AddListener(MusicVolumeChanged);
		DrumsVolumeSlider.onValueChanged.AddListener(DrumsVolumeChanged);
		SFXVolumeSlider.onValueChanged.AddListener(SFXVolumeChanged);

		InitAudioVolumes();		// from GameSettings
	}

	public void OnDisable()
	{
		BeatEvents.OnNewGame -= OnNewGame;
		BeatEvents.OnNewGameStarted -= OnRestartGame;
		BeatEvents.OnStartPlay -= OnStartPlay;
		BeatEvents.OnPlayerTap -= OnPlayerTap;

		BeatEvents.OnUpdateScore -= OnUpdateScore;
		BeatEvents.OnScoreReset -= OnScoreReset;
		BeatEvents.OnFinalGameScore -= OnFinalGameScore;
		BeatEvents.OnNewPersonalBest -= OnNewPersonalBest;
		BeatEvents.OnBonusScoreFeedback -= OnBonusScoreFeedback;
		BeatEvents.OnBonusScoreIncrement -= OnBonusScoreIncrement;
		BeatEvents.OnToggleLeaderboard -= ToggleLeaderboardsPanel;
		BeatEvents.OnFigureStateChanged -= OnFigureStateChanged;

		BeatEvents.OnZeroHealth -= OnZeroHealth;
		BeatEvents.OnZeroFuel -= OnZeroFuel;
		BeatEvents.OnZeroTime -= OnZeroTime;
		BeatEvents.OnPlayerHealthFuelChanged -= OnPlayerHealthFuel;
		BeatEvents.OnTimeRemainingChanged -= OnTimeRemainingChanged;

		OptionsButton.onClick.RemoveListener(ToggleOptionsPanel);
		LeaderboardButton.onClick.RemoveListener(OnLeaderboardButton);
		RestartButton.onClick.RemoveListener(OnRestartButton);
		ResetButton.onClick.RemoveListener(OnResetButton);
		QuitButton.onClick.RemoveListener(OnQuitButton);

		PlayAgainButton.onClick.RemoveListener(OnPlayAgain);
		StatsButton.onClick.RemoveListener(OnToggleStats);

		ConfirmResetButton.onClick.RemoveListener(ConfirmResetClicked);
		ConfirmRestartButton.onClick.RemoveListener(ConfirmRestartClicked);
		ConfirmQuitButton.onClick.RemoveListener(ConfirmQuitClicked);

		MusicVolumeSlider.onValueChanged.RemoveListener(MusicVolumeChanged);
		DrumsVolumeSlider.onValueChanged.RemoveListener(DrumsVolumeChanged);
		SFXVolumeSlider.onValueChanged.RemoveListener(SFXVolumeChanged);
	}


	private void Start()
	{
		TapToPlay.gameObject.SetActive(true);

		StoryPanel.gameObject.SetActive(true);

		//GameLogo.gameObject.SetActive(true);
		MetersPanel.gameObject.SetActive(false);

		OptionsButton.gameObject.SetActive(false);

		//CreditsPanel.gameObject.SetActive(true);
		SwipeToMove.gameObject.SetActive(false);

		RunningScorePanel.gameObject.SetActive(false);
		GameOverPanel.gameObject.SetActive(false);
		GameOverPanel.transform.localScale = Vector3.zero;
	}


	private void OnNewGame(GameScores scores)
	{
		playCount = scores.PlayCount;
		gameOver = false;

		CreditsPanel.gameObject.SetActive(false);

		ShowGameOverPanel(false);
		FadeToNewGame(scores);
	}


	private void OnRestartGame(Action restart)
	{
		FadeToWhite(restart);
	}


	private void OnFigureStateChanged(StickFigure.State oldState, StickFigure.State newState)
	{
		//State.text = newState.ToString().ToUpper();
	}

	private void OnStartPlay()
	{
		GameLogo.gameObject.SetActive(false);
		TapToPlay.gameObject.SetActive(false);

		StoryPanel.gameObject.SetActive(false);

		// 'swipe to beat' prompt for first 3 games?
		SwipeToMove.gameObject.SetActive(playCount <= SwipeToBeatPlayCount);

		ShowOptionsButton(true);

		MetersPanel.gameObject.SetActive(true);

		RunningScorePanel.gameObject.SetActive(true);
		CreditsPanel.gameObject.SetActive(false);
	}

	private void OnPlayerTap(GameManager.GameState gameState, int tapCount)
	{
		if (tapCount > 1)
			SwipeToMove.gameObject.SetActive(false);        // first tap = 'Tap to Play!'
	}

	private void OnPlayerHealthFuel(float healthChange, float healthLevel, float fuelChange, float fuelLevel)
	{
		LeanTween.value(HealthMeter.value, healthLevel, MeterUpdateTime)
					.setOnUpdate((float f) => { HealthMeter.value = f; })
					.setEaseOutQuart();

		LeanTween.value(FuelMeter.value, fuelLevel, MeterUpdateTime)
					.setOnUpdate((float f) => { FuelMeter.value = f; })
					.setEaseOutQuart();
	}

	private void OnZeroHealth()
	{
		if (!gameOver)
			GameOver();
	}

	private void OnZeroFuel()
	{
		if (!gameOver)
			GameOver();
	}

	private void OnTimeRemainingChanged(float timeChange, float timeRemaining, bool init, bool pulse)
	{
		if (init)
			TimeRemainingMeter.maxValue = timeRemaining;

		LeanTween.value(TimeRemainingMeter.value, timeRemaining, MeterUpdateTime)
					.setOnUpdate((float f) => { TimeRemainingMeter.value = f; })
					.setEaseOutQuart();

		if (pulse)
			Pulse(TimeRemainingMeter.gameObject);
	}

	private void Pulse(GameObject UIObject, Action onComplete = null)
	{
		var largeScale = Vector2.one * PulseScale;

		LeanTween.scale(UIObject.gameObject, largeScale, PulseTime)
			.setEaseInBack()
			.setOnComplete(() =>
			{
				LeanTween.scale(UIObject.gameObject, Vector2.one, PulseTime * 0.5f)
					.setEaseOutBack()
					.setOnComplete(() =>
					{
						onComplete?.Invoke();
					});
			});
	}

	private void OnZeroTime()
	{
		if (! gameOver)
			GameOver();
	}

	private void GameOver()
	{
		gameOver = true;

		GameLogo.gameObject.SetActive(true);
		MetersPanel.gameObject.SetActive(false);
		SwipeToMove.gameObject.SetActive(false);
		ShowGameOverPanel(true);
		ActivateLeaderboardPanel(true);
		ShowOptionsButton(false);
	}


	// on the beat
	private void OnUpdateScore(GameScores scores)
	{
		UpdateRunningScore(scores.RunningScore);
	}

	private void OnScoreReset()
	{
		SetScoreText(0);
	}

	private void OnFinalGameScore(GameScores scores, bool newPB, GameManager.GameOverState state)
	{
		gameOver = true;

		ZeroFuel.gameObject.SetActive(state == GameManager.GameOverState.ZeroFuel);
		ZeroHealth.gameObject.SetActive(state == GameManager.GameOverState.ZeroHealth);
		ZeroTime.gameObject.SetActive(state == GameManager.GameOverState.ZeroTime);

		// stats
		//string gamePlayTime = FormatPlayTime(scores.GamePlayTime);
		GameScores.text = String.Format("{0}\n{1}\n{2}",
										"GAME", (int)scores.GameHealthGained, (int)scores.GameFuelGained, (int)scores.GameTimeGained);

		//string PBPlayTime = FormatPlayTime(scores.PBPlayTime); 
		PBScores.text = String.Format("{0}\n{1}\n{2}",
										"BEST", (int)scores.PBHealthGained, (int)scores.GameFuelGained, (int)scores.PBTimeGained);

		// final score
		FinalScore.text = String.Format("{0:N0}", scores.RunningScore);
		FinalScore.gameObject.SetActive(true);

		ShowNewPB(newPB);
	}

	private void ShowNewPB(bool show)
	{
		if (show == newPBFlashing)		// no change!
			return;

		newPBFlashing = show;

		if (newPBFlashing)
		{
			flashNewPBCoroutine = StartCoroutine(FlashNewPB());
		}
		else
		{
			if (flashNewPBCoroutine != null)
				StopCoroutine(flashNewPBCoroutine);

			NewPersonalBest.gameObject.SetActive(false);
		}
	}

	private IEnumerator FlashNewPB()
	{
		while (newPBFlashing)
		{
			FinalScore.gameObject.SetActive(false);
			NewPersonalBest.gameObject.SetActive(true);
			yield return new WaitForSeconds(newPBFlashTime);
			FinalScore.gameObject.SetActive(true);
			NewPersonalBest.gameObject.SetActive(false);
			yield return new WaitForSeconds(newPBFlashTime);
		}

		yield return null;
	}

	private void ShowGameOverPanel(bool show)
	{
		if (show)
		{
			GameOverPanel.gameObject.SetActive(true);
			StatsPanel.gameObject.SetActive(false);

			LeanTween.scale(GameOverPanel.gameObject, Vector3.one, scoreScaleTime)
							.setEaseOutElastic();
		}
		else
		{
			LeanTween.scale(GameOverPanel.gameObject, Vector3.zero, scoreScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnComplete(() => { GameOverPanel.gameObject.SetActive(false); });

			ShowNewPB(false);
		}
	}

	private void OnBonusScoreFeedback(Color colour, Vector3 worldPosition, string message, int bonusScore)
	{
		var bonusUI = Instantiate(BonusUIPrefab, BonusScores);
		bonusUI.transform.position = Camera.main.WorldToScreenPoint(worldPosition);
		bonusUI.BonusFeedback(colour, message, bonusScore, RunningScore.transform.position);
	}

	private void OnBonusScoreIncrement(int bonusScore)
	{
		if (bonusScore > 0)
			hilightScore = true;			// for one beat only
	}


	private void OnNewPersonalBest(GameScores scores, bool reloadScores)
	{
		UpdatePersonalBest(scores.PersonalBest);
		BeatEvents.OnToggleLeaderboard?.Invoke();
	}


	private string FormatPlayTime(int playSeconds)
	{
		//Debug.Log("FormatPlayTime: playSeconds = " + playSeconds);

		int minutes = Math.Abs(playSeconds / 60);
		int hours = Math.Abs(minutes / 60);
		int remainderSecs = Math.Abs(playSeconds % 60);

		string seconds = (remainderSecs < 10) ? ("0" + remainderSecs.ToString()) : remainderSecs.ToString();
		return (hours > 0 ? (hours + ":") : "") + minutes + ":" + seconds;
	}

	private void UpdateRunningScore(long score)
	{
		SetScoreText(score);

		//RunningScore.transform.localScale = Vector3.one;

		//var outScale = Vector3.one * PulseScale;
		//var inScale = RunningScorePanel.transform.localScale;

		//if (hilightScore)
		//{
		//	outScale *= scoreHilightScaleFactor;
		//	hilightScore = false;
		//}
	}

	private void UpdatePersonalBest(long newPB)
	{
		PBScore.transform.localScale = Vector3.one;

		var outScale = Vector3.one * NewPBPulseScale;
		var inScale = PBScore.transform.localScale;

		LeanTween.scale(PBScore.gameObject, outScale, NewPBPulseTime)
					.setEaseInBack()
					.setOnComplete(() =>
					{
						SetPBText(newPB);

						LeanTween.scale(PBScore.gameObject, inScale, NewPBPulseTime)
									.setEaseOutBack();
					});
	}


	private void SetScoreText(long score)
	{
		RunningScore.text = String.Format("SCORE: {0:N0}", score);

		Pulse(RunningScore.gameObject);
	}

	private void SetPBText(long PB)
	{
		PBScore.text = String.Format("BEST: {0:N0}", PB);
	}


	private void FadeToNewGame(GameScores scores, Action onBlackOut = null)
	{
		Blackout.color = Color.clear;
		Blackout.gameObject.SetActive(true);

		LeanTween.color(Blackout.gameObject, BlackOutColour, fadeToBlackTime)
				.setOnUpdate((Color c) => { Blackout.color = c; })
				.setEaseOutSine()
				.setOnComplete(() =>
				{
					onBlackOut?.Invoke();

					SetPBText(scores.PersonalBest);
					SetScoreText(0);

					BeatEvents.OnStartPlay?.Invoke();

					LeanTween.color(Blackout.gameObject, Color.clear, fadeToBlackTime * 1.5f)
							.setOnUpdate((Color c) => { Blackout.color = c; })
							.setEaseInSine()
							.setOnComplete(() =>
							{
								Blackout.gameObject.SetActive(false);
							});
				});
	}


	private void FadeToWhite(Action onWhiteOut = null)
	{
		Blackout.color = Color.clear;
		Blackout.gameObject.SetActive(true);

		LeanTween.color(Blackout.gameObject, WhiteOutColour, fadeToWhiteTime)
				.setOnUpdate((Color c) => { Blackout.color = c; })
				.setEaseOutCubic()
				.setOnComplete(() =>
				{
					onWhiteOut?.Invoke();

					LeanTween.color(Blackout.gameObject, Color.clear, fadeToWhiteTime * 1.5f)
							.setOnUpdate((Color c) => { Blackout.color = c; })
							.setEaseInCubic()
							.setOnComplete(() =>
							{
								Blackout.gameObject.SetActive(false);
							});
				});
	}

	/// <summary>
	/// Converts the altitude into a degree rotation between -90 and 90 for use with the altimeter
	/// </summary>
	/// <returns>Returns the value of the rotation</returns>
	private float ConvertAltToRotation(float currentAltitude, float maxAltitude)
    {
		return Mathf.Clamp(((currentAltitude / maxAltitude * 180) - 90) * -1, -90f, 90f);
    }


	private void ToggleOptionsPanel()
	{
		optionsPanelShowing = !optionsPanelShowing;
		BeatEvents.OnGameOptions?.Invoke(optionsPanelShowing);

		ScaleOptionsPanel(optionsPanelShowing);

		if (!optionsPanelShowing)
			ActivateLeaderboardPanel(false);

		MetersPanel.gameObject.SetActive(!optionsPanelShowing);
	}

	private void ScaleOptionsPanel(bool showing)
	{
		if (showing)
		{
			OptionsPanel.alpha = 0f;
			OptionsPanel.transform.localScale = Vector3.zero;
			OptionsPanel.gameObject.SetActive(true);

			ResetButtons();

			LeanTween.scale(OptionsPanel.gameObject, Vector3.one, optionsScaleTime)
							.setEaseOutElastic();

			LeanTween.value(OptionsPanel.gameObject, 0f, 1f, optionsScaleTime)
							.setEaseOutElastic()
							.setOnUpdate((float f) => { OptionsPanel.alpha = f; });
		}
		else
		{
			LeanTween.scale(OptionsPanel.gameObject, Vector3.zero, optionsScaleTime * 0.5f)
							.setEaseInCubic();

			LeanTween.value(OptionsPanel.gameObject, 1f, 0f, optionsScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnUpdate((float f) => { OptionsPanel.alpha = f; })
							.setOnComplete(() =>
							{
								OptionsPanel.gameObject.SetActive(false);
							});
		}
	}

	private void ResetButtons()
	{
		RestartButton.gameObject.SetActive(true);
		RestartButton.interactable = true;
		ConfirmRestartButton.gameObject.SetActive(false);

		ResetButton.gameObject.SetActive(true);
		ResetButton.interactable = true;
		ConfirmResetButton.gameObject.SetActive(false);

		QuitButton.gameObject.SetActive(true);
		QuitButton.interactable = true;
		ConfirmQuitButton.gameObject.SetActive(false);
	}

	private void ToggleLeaderboardsPanel()
	{
		ActivateLeaderboardPanel(!leaderboardPanelShowing);
	}

	private void ActivateLeaderboardPanel(bool showing)
	{
		if (showing == leaderboardPanelShowing)
			return;

		CanvasGroup LeaderboardPanel = LeaderboardUI.GetComponent<CanvasGroup>();

		if (showing)
		{
			LeaderboardUI.Populate();		// player name/school, schools, top scores

			LeaderboardPanel.alpha = 0f;
			LeaderboardPanel.transform.localScale = Vector3.zero;
			LeaderboardPanel.gameObject.SetActive(true);

			LeanTween.scale(LeaderboardPanel.gameObject, Vector3.one, optionsScaleTime)
							.setEaseOutElastic();

			LeanTween.value(LeaderboardPanel.gameObject, 0f, 1f, optionsScaleTime)
							.setEaseOutElastic()
							.setOnUpdate((float f) => { LeaderboardPanel.alpha = f; })
							.setOnComplete(() =>
							{
								leaderboardPanelShowing = true;
							});
		}
		else
		{
			LeaderboardUI.CancelPopulate();       // cancel score retrieval

			LeanTween.scale(LeaderboardPanel.gameObject, Vector3.zero, optionsScaleTime * 0.5f)
							.setEaseInCubic();

			LeanTween.value(LeaderboardPanel.gameObject, 1f, 0f, optionsScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnUpdate((float f) => { LeaderboardPanel.alpha = f; })
							.setOnComplete(() =>
							{
								LeaderboardPanel.gameObject.SetActive(false);
								leaderboardPanelShowing = false;
							});
		}
	}

	private void ShowOptionsButton(bool showing)
	{
		if (showing)
		{
			OptionsButton.transform.localScale = Vector3.zero;
			OptionsButton.gameObject.SetActive(true);

			LeanTween.scale(OptionsButton.gameObject, Vector3.one, optionsScaleTime)
							.setEaseOutElastic();
		}
		else
		{
			LeanTween.scale(OptionsButton.gameObject, Vector3.zero, optionsScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnComplete(() =>
							{
								OptionsButton.gameObject.SetActive(false);
							});
		}
	}

	private void InitAudioVolumes()
	{
		MusicVolumeSlider.value = GameSettings.MusicVolume;
		DrumsVolumeSlider.value = GameSettings.DrumsVolume;
		SFXVolumeSlider.value = GameSettings.SFXVolume;

		BeatEvents.OnMusicVolumeChanged?.Invoke(GameSettings.MusicVolume);
		BeatEvents.OnDrumsVolumeChanged?.Invoke(GameSettings.DrumsVolume);
		BeatEvents.OnSFXVolumeChanged?.Invoke(GameSettings.SFXVolume);
	}

	private void MusicVolumeChanged(float vol)
	{
		GameSettings.MusicVolume = vol;
		BeatEvents.OnMusicVolumeChanged?.Invoke(vol);
	}

	private void DrumsVolumeChanged(float vol)
	{
		GameSettings.DrumsVolume = vol;
		BeatEvents.OnDrumsVolumeChanged?.Invoke(vol);

	}

	private void SFXVolumeChanged(float vol)
	{
		GameSettings.SFXVolume = vol;
		BeatEvents.OnDrumsVolumeChanged?.Invoke(vol);
	}


	private void OnLeaderboardButton()
	{
		BeatEvents.OnToggleLeaderboard?.Invoke();
	}


	private void OnPlayAgain()
	{
		ShowGameOverPanel(false);
		ActivateLeaderboardPanel(false);

		BeatEvents.OnStartNewGame?.Invoke();
	}


	private void OnToggleStats()
	{
		bool show = ! StatsPanel.gameObject.activeSelf;

		//StatsButtonText.text = show ? "HIDE STATS" : "SHOW STATS";

		if (show)
		{
			StatsPanel.alpha = 0f;
			StatsPanel.transform.localScale = Vector3.zero;
			StatsPanel.gameObject.SetActive(true);

			LeanTween.scale(StatsPanel.gameObject, Vector3.one, scoreScaleTime)
							.setEaseOutElastic();

			LeanTween.value(StatsPanel.gameObject, 0f, 1f, scoreScaleTime)
							.setEaseOutElastic()
							.setOnUpdate((float f) => { StatsPanel.alpha = f; });
		}
		else
		{
			LeanTween.scale(StatsPanel.gameObject, Vector3.zero, scoreScaleTime * 0.5f)
							.setEaseInCubic();

			LeanTween.value(StatsPanel.gameObject, 1f, 0f, scoreScaleTime * 0.5f)
							.setEaseInCubic()
							.setOnUpdate((float f) => { StatsPanel.alpha = f; })
							.setOnComplete(() => StatsPanel.gameObject.SetActive(false));
		}
	}


	private void OnRestartButton()
	{
		StartCoroutine(ConfirmRestartTimer());
	}

	private void ConfirmRestartClicked()
	{
		ToggleOptionsPanel();       // hide options
		BeatEvents.OnStartNewGame?.Invoke();

		RestartButton.gameObject.SetActive(true);
		ConfirmRestartButton.gameObject.SetActive(false);

		ResetButton.interactable = true;
		QuitButton.interactable = true;
	}

	private IEnumerator ConfirmRestartTimer()
	{
		RestartButton.gameObject.SetActive(false);
		ConfirmRestartButton.gameObject.SetActive(true);

		ResetButton.interactable = false;
		QuitButton.interactable = false;

		yield return new WaitForSeconds(ConfirmTimeout);

		RestartButton.gameObject.SetActive(true);
		ConfirmRestartButton.gameObject.SetActive(false);

		ResetButton.interactable = true;
		QuitButton.interactable = true;

		yield return null;
	}

	private void OnResetButton()
	{
		StartCoroutine(ConfirmResetTimer());
	}

	private void ConfirmResetClicked()
	{
		ToggleOptionsPanel();       // hide options
		BeatEvents.OnResetData?.Invoke();

		// restart after reset
		BeatEvents.OnStartNewGame?.Invoke();

		ResetButton.gameObject.SetActive(true);
		ConfirmResetButton.gameObject.SetActive(false);

		RestartButton.interactable = true;
		QuitButton.interactable = true;
	}

	private IEnumerator ConfirmResetTimer()
	{
		ResetButton.gameObject.SetActive(false);
		ConfirmResetButton.gameObject.SetActive(true);

		RestartButton.interactable = false;
		QuitButton.interactable = false;

		yield return new WaitForSeconds(ConfirmTimeout);

		ResetButton.gameObject.SetActive(true);
		ConfirmResetButton.gameObject.SetActive(false);

		RestartButton.interactable = true;
		QuitButton.interactable = true;

		yield return null;
	}

	private void OnQuitButton()
	{
		StartCoroutine(ConfirmQuitTimer());
	}

	private void ConfirmQuitClicked()
	{
		ToggleOptionsPanel();      // hide options
		BeatEvents.OnQuitGame?.Invoke();
		Application.Quit();

		QuitButton.gameObject.SetActive(true);
		ConfirmQuitButton.gameObject.SetActive(false);

		RestartButton.interactable = true;
		ResetButton.interactable = true;
	}

	private IEnumerator ConfirmQuitTimer()
	{
		QuitButton.gameObject.SetActive(false);
		ConfirmQuitButton.gameObject.SetActive(true);

		RestartButton.interactable = false;
		ResetButton.interactable = false;

		yield return new WaitForSeconds(ConfirmTimeout);

		QuitButton.gameObject.SetActive(true);
		ConfirmQuitButton.gameObject.SetActive(false);

		RestartButton.interactable = true;
		ResetButton.interactable = true;

		yield return null;
	}
}
