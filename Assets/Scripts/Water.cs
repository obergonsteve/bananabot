﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
	public AudioClip SplashAudio;
	public ParticleSystem EntrySplash;
	public ParticleSystem ExitSplash;

	//private float splashZOffset = 2f;
	//private Vector3 entryPosition;

	public void OnEnable()
	{
		BeatEvents.OnFigurePositionChanged += OnFigurePositionChanged;
		BeatEvents.OnFigureHitWater += OnFigureHitWater;
		BeatEvents.OnFigureLeaveWater += OnFigureLeaveWater;
	}

	public void OnDisable()
	{
		BeatEvents.OnFigurePositionChanged -= OnFigurePositionChanged;
		BeatEvents.OnFigureHitWater -= OnFigureHitWater;
		BeatEvents.OnFigureLeaveWater -= OnFigureLeaveWater;
	}


	// move water left/right to be directly under stick figure
	private void OnFigurePositionChanged(Vector2 lastPosition, Vector2 figurePosition)
	{
		transform.position = new Vector3(figurePosition.x, transform.position.y, transform.position.z);
	}

	private void OnFigureHitWater(Vector3 figurePosition)
	{
		PlaySplashAudio();
		EntrySplash.transform.position = new Vector3(figurePosition.x, EntrySplash.transform.position.y, EntrySplash.transform.position.z);
		EntrySplash.Play();
	}

	private void OnFigureLeaveWater(Vector3 figurePosition)
	{
		PlaySplashAudio();
		ExitSplash.transform.position = new Vector3(figurePosition.x, ExitSplash.transform.position.y, ExitSplash.transform.position.z);
		ExitSplash.Play();
	}


	private void PlaySplashAudio()
	{
		if (SplashAudio != null)
			AudioSource.PlayClipAtPoint(SplashAudio, transform.position, AudioManager.SFXVolume);
	}
}
